#!/bin/bash
VERSION=$MAJOR.$MINOR.$COMMITCOUNT
if [ ${#VERSION} -le 2 ]; then
    #this happens when no env variable is set
    VERSION=$(git rev-parse --short HEAD)
fi

mkdir -p bin

CGO_ENABLED=0

go build -ldflags "-X 'gitlab.com/loranna/loranna/command/client.version=$VERSION'" \
-o ./bin/loranna ./cmd/loranna

go build -ldflags "-X 'gitlab.com/loranna/loranna/command/daemon.version=$VERSION'" \
-o ./bin/lorannad ./cmd/lorannad
