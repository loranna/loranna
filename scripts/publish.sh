#!/bin/bash


TAG=$(git tag -l --sort=-v:refname | head -n 1 | cut -c 2-)
IFS='.' read -ra vers <<< "$TAG"
MAJOR="${vers[0]}"
MINOR="${vers[1]}"
PATCH="${vers[2]}"

VERSION=$MAJOR.$MINOR.$PATCH
#package_cloud push tkiraly/loranna/ubuntu/bionic packages/loranna_${VERSION}_amd64.deb

#curl -X POST https://slack.com/api/chat.postMessage \
#-H "Content-type: application/json;charset=utf-8" \
#-H "Authorization: Bearer $SLACK_TOKEN" \
#-d  "{\"channel\":\"$SLACK_CHANNEL\",\"text\":\"new version of loranna is released: $VERSION\"}"
#
#curl -F file=@packages/loranna_${VERSION}_amd64.deb -F channels="$SLACK_CHANNEL" \
#-H "Authorization: Bearer $SLACK_TOKEN" \
#https://slack.com/api/files.upload