#!/bin/bash

rm *.deb

TAG=$(git tag -l --sort=-v:refname | head -n 1 | cut -c 2-)
IFS='.' read -ra vers <<< "$TAG"
MAJOR="${vers[0]}"
MINOR="${vers[1]}"
PATCH="${vers[2]}"

VERSION=$MAJOR.$MINOR.$PATCH

fpm -s dir -t deb\
 -v ${VERSION}\
 -n loranna\
 --after-remove ./scripts/package/after-remove.sh\
 --deb-systemd systemd/loranna.service\
 --depends libprotobuf-c1>=1.3.2\
 --depends librabbitmq4>=0.9.0\
 --maintainer "Tamás Király <kiraly.tamas@outlook.com>"\
 --vendor "Tamás Király <kiraly.tamas@outlook.com>"\
 --url https://gitlab.com/loranna\
 --description "Manages lorawan network simulations."\
 --license MIT\
 ./bin=/usr/

mkdir -p packages
 mv *.deb packages/