#!/bin/bash

go run ./docgen
docker run --rm -v "`pwd`:/data" --user `id -u`:`id -g` \
pandoc/latex:2.9.2 README.md ARCHITECTURE.md Commands.md docs/*.md -o README.pdf --toc \
--top-level-division=section --listings -H docs/listings-setup.tex --number-sections

docker run --rm -v "`pwd`:/data" --user `id -u`:`id -g` \
pandoc/latex:2.9.2 CHANGELOG.md -o CHANGELOG.pdf --toc \
--top-level-division=section --listings -H docs/listings-setup.tex

mkdir -p pdf
mv *.pdf pdf/

#curl -F file=@pdf/CHANGELOG.pdf -F channels="$SLACK_CHANNEL" \
#-H "Authorization: Bearer $SLACK_TOKEN" \
#https://slack.com/api/files.upload
#
#curl -F file=@pdf/README.pdf -F channels="$SLACK_CHANNEL" \
#-H "Authorization: Bearer $SLACK_TOKEN" \
#https://slack.com/api/files.upload