#!/bin/bash

TAG=$(git tag -l --sort=-v:refname | head -n 1 | cut -c 2-)
IFS='.' read -ra vers <<< "$TAG"
MAJOR="${vers[0]}"
MINOR="${vers[1]}"
PATCH="${vers[2]}"

VERSION=$MAJOR.$MINOR.$COMMITCOUNT

mkdir -p bin

CGO_ENABLED=0

go build -ldflags "-X 'gitlab.com/loranna/loranna/command/client.version=$VERSION'" \
-o ./bin/loranna ./cmd/loranna

go build -ldflags "-X 'gitlab.com/loranna/loranna/command/daemon.version=$VERSION'" \
-o ./bin/lorannad ./cmd/lorannad
