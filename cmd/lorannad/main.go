package main

import (
	"math/rand"
	"time"

	"github.com/sirupsen/logrus"
	cmd "gitlab.com/loranna/loranna/command/daemon/cmd"
)

func main() {
	logrus.SetLevel(logrus.TraceLevel)
	rand.Seed(time.Now().UnixNano())
	cmd.Execute()
}
