module gitlab.com/loranna/loranna

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	github.com/sirupsen/logrus v1.5.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.6.3
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	gitlab.com/loranna/configuration v0.0.0-20200926130436-db4810b79a9e
	gitlab.com/loriot/api v0.2.27
	gopkg.in/yaml.v2 v2.2.4
)
