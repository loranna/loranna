package loriot

import (
	"fmt"
	"strings"

	"gitlab.com/loriot/api/core"
)

// RegisterGateway registers a gateway in loriot with taking the following steps:
//
// - checks if the gateway is already registered or not
//
// - if not, it tries to register the gateway in a network called loranna
//
// - if no loranna named network exist, it tries to create one and add the gateway to it
func RegisterGateway(url, apikey, mac string) error {
	client := core.NewClient(url, apikey)

	//lets check if gateway is accessible with the api key (registered)
	nwks, err := client.NetworkList()
	if err != nil {
		return err
	}
	for _, nwk := range nwks {
		gws, err := client.GatewaysList(fmt.Sprintf("%08X", nwk.ID))
		if err != nil {
			return err
		}
		for _, gw := range gws {
			if strings.ToUpper(gw.MAC) == strings.ToUpper(mac) {
				return nil
			}
		}
	}
	//lets check if we have a network with name loranna
	for _, nwk := range nwks {
		if strings.ToLower(nwk.Name) == "loranna" {
			err := client.GatewayAdd(fmt.Sprintf("%08X", nwk.ID), mac)
			if err != nil {
				return err
			}
			return nil
		}
	}
	// create network because we don't have it
	err = client.NetworkAdd("loranna")
	if err != nil {
		return err
	}
	nwks, err = client.NetworkList()
	if err != nil {
		return err
	}
	for _, nwk := range nwks {
		if strings.ToLower(nwk.Name) == "loranna" {
			err := client.GatewayAdd(fmt.Sprintf("%08X", nwk.ID), mac)
			if err != nil {
				return err
			}
			return nil
		}
	}
	return nil
}

// RegisterABPDevice registers an ABP device in loriot with taking the following steps:
//
// - checks if the device is already registered or not
//
// - if not, it tries to register the ABP device in an application called loranna
//
// - if no loranna named application exist, it tries to create one and add the ABP device to it
func RegisterABPDevice(url, apikey, deveui, devaddr, nwkskey, appskey, title string, cntup, cntdn uint) error {
	client := core.NewClient(url, apikey)

	//lets check if device is accessible with the api key (registered)
	apps, err := client.ApplicationList()
	if err != nil {
		return err
	}
	for _, app := range apps {
		devs, err := client.DevicesList(fmt.Sprintf("%08X", app.ID))
		if err != nil {
			return err
		}
		for _, dev := range devs {
			if strings.ToUpper(dev.ID) == strings.ToUpper(deveui) {
				return nil
			}
		}
	}
	//lets check if we have an application with name loranna
	for _, app := range apps {
		if strings.ToLower(app.Name) == "loranna" {
			err := client.DeviceAbpAdd(fmt.Sprintf("%08X", app.ID), deveui, devaddr, nwkskey, appskey, title, int(cntup), int(cntdn))
			if err != nil {
				return err
			}
			return nil
		}
	}
	// create application because we don't have it
	err = client.ApplicationAdd("loranna")
	if err != nil {
		return err
	}
	apps, err = client.ApplicationList()
	if err != nil {
		return err
	}
	for _, app := range apps {
		if strings.ToLower(app.Name) == "loranna" {
			err := client.DeviceAbpAdd(fmt.Sprintf("%08X", app.ID), deveui, devaddr, nwkskey, appskey, title, int(cntup), int(cntdn))
			if err != nil {
				return err
			}
			return nil
		}
	}
	return nil
}

// RegisterOTAADevice registers an OTAA device in loriot with taking the following steps:
//
// - checks if the device is already registered or not
//
// - if not, it tries to register the OTAA device in an application called loranna
//
// - if no loranna named application exist, it tries to create one and add the OTAA device to it
func RegisterOTAADevice(url, apikey, deveui, appeui, appkey, title string) error {
	client := core.NewClient(url, apikey)

	//lets check if device is accessible with the api key (registered)
	apps, err := client.ApplicationList()
	if err != nil {
		return err
	}
	for _, app := range apps {
		devs, err := client.DevicesList(fmt.Sprintf("%08X", app.ID))
		if err != nil {
			return err
		}
		for _, dev := range devs {
			if strings.ToUpper(dev.ID) == strings.ToUpper(deveui) {
				return nil
			}
		}
	}
	//lets check if we have an application with name loranna
	for _, app := range apps {
		if strings.ToLower(app.Name) == "loranna" {
			err := client.DeviceOtaaAdd(fmt.Sprintf("%08X", app.ID), deveui, appeui, appkey, title)
			if err != nil {
				return err
			}
			return nil
		}
	}
	// create application because we don't have it
	err = client.ApplicationAdd("loranna")
	if err != nil {
		return err
	}
	apps, err = client.ApplicationList()
	if err != nil {
		return err
	}
	for _, app := range apps {
		if strings.ToLower(app.Name) == "loranna" {
			err := client.DeviceOtaaAdd(fmt.Sprintf("%08X", app.ID), deveui, appeui, appkey, title)
			if err != nil {
				return err
			}
			return nil
		}
	}
	return nil
}
