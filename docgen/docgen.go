package main

import (
	"gitlab.com/loranna/loranna/command/client/cmd"
	cmd2 "gitlab.com/loranna/loranna/command/daemon/cmd"
)

func main() {
	err := cmd.GenerateDocumentation()
	if err != nil {
		panic(err)
	}
	err = cmd2.GenerateDocumentation()
	if err != nil {
		panic(err)
	}
}
