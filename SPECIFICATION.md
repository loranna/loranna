# Specification

This document describes the loranna architecture, components and messages between them. Loranna is a highly configurable full lorawan network simulator

## Components

RabbitMQ sits in the middle as the message broker.

``` bash
+------------+   +----------------+   +-------------+
|            |   |                |   |             |
|            |   |                |   |             |
|   Device   +---+    RabbitMQ    +---+   Gateway   +---> LNS
|            |   |                |   |             |
|            |   |                |   |             |
+------------+   +-------+--------+   +-------------+
                         |
                         |
                         |
                 +-------+--------+
                 |                |
                 |                |
                 |  Environment   |
                 |                |
                 |                |
                 +----------------+
```

- device
- environment
- gateway

### Device

Device simulates a lorawan capable device sending and receiving data with the specified interval.

### Environment

Environment routes uplinks and downlinks betwen devices and gateways. It also sets radio signal parameters

### Gateway

Gateway interprets the internal protocol as another one which can be understood by the LNS