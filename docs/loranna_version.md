## loranna version

Show version number

### Synopsis

Show version number

```
loranna version [flags]
```

### Options

```
  -h, --help   help for version
```

### SEE ALSO

* [loranna](#loranna)	 - manages a loranna simulation

