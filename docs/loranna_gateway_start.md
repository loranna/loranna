## loranna gateway start

Starts the gateway using the default or the supplied config file

### Synopsis

Starts the gateway using the default or the supplied config file

```
loranna gateway start [filename] [flags]
```

### Options

```
      --deviceupqueuename string      Name of the queue used for the devie uplink to the environment messages (default "uplinktoenvironment")
      --duplexity string              duplexity of the gateway. Possible values: fullduplex,halfduplex (default "fullduplex")
      --environmentserverport int     Port of the environment used to manipulate messages in both directions (default 35999)
      --environmentserverurl string   URL of the environment used to manipulate messages in both directions
      --expectedlatency float         Expected Latency of the gateway
      --expectedrssi float            Value of the expected RSSI attached to uplinks (default -55)
      --expectedsnr float             Value of the expected SNR attached to uplinks
      --gatewaydownqueuename string   Name of the queue used for the gateway downlink to the environment messages (default "downlinktoenvironment")
  -h, --help                          help for start
      --keepconfig                    Keep the configuration file(s) after using them
      --latitude float                Latitude of the gateway (default 19.078076)
      --longitude float               Longitude of the gateway (default 47.515086)
      --loriotserverurl string        URL of the LORIOT server used for data processing
      --macaddress string             MAC/HW address of the gateway
      --networkinterfacename string   Name of the default interface used by the gateway
      --rabbitmqpassword string       Password of the RabbitMQ server used for data exchange (default "guest")
      --rabbitmqport int              Port of the RabbitMQ server used for data exchange (default 5672)
      --rabbitmqserverurl string      URL of the RabbitMQ server used for data exchange (default "localhost")
      --rabbitmqusername string       User Name of the RabbitMQ server used for data exchange (default "guest")
      --radiomodel string             radio model of the gateway. Possible values: simple (default "simple")
      --rssibias float                Bias of the attached RSSI (default 2)
      --snrbias float                 Bias of the attached SNR (default 1)
```

### Options inherited from parent commands

```
      --runner string          Sets the runner to be used for the gateway. Possible values: native,docker (default "native")
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna gateway](#loranna-gateway)	 - gateway related commands

