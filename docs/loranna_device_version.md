## loranna device version

Queries the version of the device

### Synopsis

Queries the version of the device

```
loranna device version [flags]
```

### Options

```
  -h, --help   help for version
```

### Options inherited from parent commands

```
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna device](#loranna-device)	 - devices related commands

