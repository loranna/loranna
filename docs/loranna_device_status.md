## loranna device status

Queries the status of device(s).

### Synopsis

Queries all devices' status, when no parameter supplied
and returns one device's status when id is utilized

```
loranna device status [id] [flags]
```

### Options

```
  -h, --help   help for status
```

### Options inherited from parent commands

```
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna device](#loranna-device)	 - devices related commands

