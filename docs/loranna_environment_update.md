## loranna environment update

Updates the environment component

### Synopsis

Updates the environment component

```
loranna environment update [version] [flags]
```

### Options

```
  -h, --help   help for update
```

### Options inherited from parent commands

```
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna environment](#loranna-environment)	 - environment related commands

