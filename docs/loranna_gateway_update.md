## loranna gateway update

Updates the version of the gateway component

### Synopsis

Updates the version of the gateway component

```
loranna gateway update [version] [flags]
```

### Options

```
  -h, --help   help for update
```

### Options inherited from parent commands

```
      --runner string          Sets the runner to be used for the gateway. Possible values: native,docker (default "native")
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna gateway](#loranna-gateway)	 - gateway related commands

