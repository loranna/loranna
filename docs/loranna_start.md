## loranna start

manages a loranna simulation

### Synopsis

manages a loranna simulation

```
loranna start [flags]
```

### Options

```
  -h, --help                   help for start
      --serveraddress string   URL of the Lorannad server
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna](#loranna)	 - manages a loranna simulation

