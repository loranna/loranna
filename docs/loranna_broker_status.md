## loranna broker status

Queries the status of the broker

### Synopsis

Queries the status of the broker

```
loranna broker status [flags]
```

### Options

```
  -h, --help   help for status
```

### Options inherited from parent commands

```
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna broker](#loranna-broker)	 - broker related commands

