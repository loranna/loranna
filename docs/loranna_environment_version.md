## loranna environment version

Queries the version of the environment

### Synopsis

Queries the version of the environment

```
loranna environment version [flags]
```

### Options

```
  -h, --help   help for version
```

### Options inherited from parent commands

```
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna environment](#loranna-environment)	 - environment related commands

