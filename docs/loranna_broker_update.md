## loranna broker update

Updates the broker

### Synopsis

Updates the broker

```
loranna broker update version [flags]
```

### Options

```
  -h, --help                       help for update
      --rabbitmqpassword string    Password of the RabbitMQ server used for data exchange (default "guest")
      --rabbitmqport int           Port of the RabbitMQ server used for data exchange (default 5672)
      --rabbitmqserverurl string   URL of the RabbitMQ server used for data exchange (default "localhost")
      --rabbitmqusername string    User Name of the RabbitMQ server used for data exchange (default "guest")
```

### Options inherited from parent commands

```
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna broker](#loranna-broker)	 - broker related commands

