## loranna broker stop

Stops the broker

### Synopsis

Stops the broker

```
loranna broker stop [flags]
```

### Options

```
  -h, --help   help for stop
```

### Options inherited from parent commands

```
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna broker](#loranna-broker)	 - broker related commands

