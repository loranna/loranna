## loranna gateway stop

Stops the gateway

### Synopsis

Stops the gateway

```
loranna gateway stop [flags]
```

### Options

```
  -h, --help   help for stop
```

### Options inherited from parent commands

```
      --runner string          Sets the runner to be used for the gateway. Possible values: native,docker (default "native")
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna gateway](#loranna-gateway)	 - gateway related commands

