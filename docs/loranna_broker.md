## loranna broker

broker related commands

### Synopsis

broker related commands

### Options

```
  -h, --help   help for broker
```

### Options inherited from parent commands

```
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna](#loranna)	 - manages a Loranna simulation
* [loranna broker start](#loranna-broker-start)	 - Starts broker in the server
* [loranna broker status](#loranna-broker-status)	 - Queries the status of the broker
* [loranna broker stop](#loranna-broker-stop)	 - Stops the broker
* [loranna broker update](#loranna-broker-update)	 - Updates the broker

