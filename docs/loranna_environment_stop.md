## loranna environment stop

Stops the environment

### Synopsis

Stops the environment

```
loranna environment stop [flags]
```

### Options

```
  -h, --help   help for stop
```

### Options inherited from parent commands

```
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna environment](#loranna-environment)	 - environment related commands

