## loranna device stop

Stops the selected or all devices

### Synopsis

Stops the selected or all devices

```
loranna device stop [id] [flags]
```

### Options

```
  -h, --help   help for stop
```

### Options inherited from parent commands

```
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna device](#loranna-device)	 - devices related commands

