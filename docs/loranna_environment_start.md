## loranna environment start

Starts the environment

### Synopsis

Starts the environment

```
loranna environment start [flags]
```

### Options

```
      --deviceupqueuename string      Name of the queue used for the devie uplink to the environment messages (default "uplinktoenvironment")
      --environmentserverport int     Port of the environment used to manipulate messages in both directions (default 35999)
      --environmentserverurl string   URL of the environment used to manipulate messages in both directions
      --gatewaydownqueuename string   Name of the queue used for the gateway downlink to the environment messages (default "downlinktoenvironment")
  -h, --help                          help for start
      --rabbitmqpassword string       Password of the RabbitMQ server used for data exchange (default "guest")
      --rabbitmqport int              Port of the RabbitMQ server used for data exchange (default 5672)
      --rabbitmqserverurl string      URL of the RabbitMQ server used for data exchange (default "localhost")
      --rabbitmqusername string       User Name of the RabbitMQ server used for data exchange (default "guest")
```

### Options inherited from parent commands

```
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna environment](#loranna-environment)	 - environment related commands

