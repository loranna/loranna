## loranna gateway

gateway related commands

### Synopsis

gateway related commands

### Options

```
  -h, --help            help for gateway
      --runner string   Sets the runner to be used for the gateway. Possible values: native,docker (default "native")
```

### Options inherited from parent commands

```
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna](#loranna)	 - manages a Loranna simulation
* [loranna gateway register](#loranna-gateway-register)	 - Registers a gateway in LORIOT with the MAC address returned by the server
* [loranna gateway start](#loranna-gateway-start)	 - Starts the gateway using the default or the supplied config file
* [loranna gateway stop](#loranna-gateway-stop)	 - Stops the gateway
* [loranna gateway update](#loranna-gateway-update)	 - Updates the version of the gateway component
* [loranna gateway version](#loranna-gateway-version)	 - Queries the version of the gateway component

