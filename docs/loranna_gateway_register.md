## loranna gateway register

Registers a gateway in LORIOT with the MAC address returned by the server

### Synopsis

Registers a gateway in LORIOT with the MAC address returned by the server

```
loranna gateway register [flags]
```

### Options

```
  -h, --help                        help for register
      --loriotserverapikey string   API key of the LORIOT server used for data processing
      --loriotserverurl string      URL of the LORIOT server used for data processing
```

### Options inherited from parent commands

```
      --runner string          Sets the runner to be used for the gateway. Possible values: native,docker (default "native")
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna gateway](#loranna-gateway)	 - gateway related commands

