## loranna device

devices related commands

### Synopsis

devices related commands

### Options

```
  -h, --help   help for device
```

### Options inherited from parent commands

```
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna](#loranna)	 - manages a Loranna simulation
* [loranna device register](#loranna-device-register)	 - Registers device(s)
* [loranna device start](#loranna-device-start)	 - Starts devices based on the existing config file(s)
* [loranna device status](#loranna-device-status)	 - Queries the status of device(s).
* [loranna device stop](#loranna-device-stop)	 - Stops the selected or all devices
* [loranna device update](#loranna-device-update)	 - Updates the version of the device component
* [loranna device version](#loranna-device-version)	 - Queries the version of the device

