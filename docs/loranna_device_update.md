## loranna device update

Updates the version of the device component

### Synopsis

Updates the version of the device component

```
loranna device update [version] [flags]
```

### Options

```
  -h, --help   help for update
```

### Options inherited from parent commands

```
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna device](#loranna-device)	 - devices related commands

