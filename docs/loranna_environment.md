## loranna environment

environment related commands

### Synopsis

environment related commands

### Options

```
  -h, --help   help for environment
```

### Options inherited from parent commands

```
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna](#loranna)	 - manages a Loranna simulation
* [loranna environment start](#loranna-environment-start)	 - Starts the environment
* [loranna environment stop](#loranna-environment-stop)	 - Stops the environment
* [loranna environment update](#loranna-environment-update)	 - Updates the environment component
* [loranna environment version](#loranna-environment-version)	 - Queries the version of the environment

