## loranna device register

Registers device(s)

### Synopsis

This command can register one or more devices. If count is not supplied it
will register a device in the supplied server using the supplied API key.
Otherwise, in batch mode, it will register count number of devices with
random unique values hence the following flags are illegal in this mode:
deveui, appeui, appkey, devaddr, appskey, nwkskey, deviceserverport

```
loranna device register [count] [flags]
```

### Options

```
      --adr                           Enable/Disable the ADR (Adaptive Data Rate) in the device (default true)
      --appeui string                 Appeui of the device according to LoRaWAN specification (default "7AAAA5407066198F")
      --appkey string                 Appkey of the device according to LoRaWAN specification (default "D1AC7013C3D24656FFFC10831AF59E26")
      --appskey string                Appskey of the device according to LoRaWAN specification (default "CBC13222DCAB8660AA9990A40F759159")
      --counterdown int               Value of the downlink counter (FCntDown) in the device side
      --counterup int                 Value of the uplink counter (FCntUp) in the device side
      --devaddr string                Devaddr of the device according to LoRaWAN specification (default "520CC433")
      --deveui string                 Deveui of the device according to LoRaWAN specification (default "7AAAA5EB18744762")
      --deviceserverport int          Port of the device used to access it during execution (default 36000)
      --deviceupqueuename string      Name of the queue used for the devie uplink to the environment messages (default "uplinktoenvironment")
      --environmentserverport int     Port of the environment used to manipulate messages in both directions (default 35999)
      --environmentserverurl string   URL of the environment used to manipulate messages in both directions
      --gatewaydownqueuename string   Name of the queue used for the gateway downlink to the environment messages (default "downlinktoenvironment")
  -h, --help                          help for register
      --joinmethod string             Join method of the device according to LoRaWAN specification. Possible values: OTAA,ABP (default "OTAA")
      --latitude float                Latitude of the device (default 19.078076)
      --longitude float               Longitude of the device (default 47.515086)
      --loriotserverapikey string     API key of the LORIOT server used for data processing
      --loriotserverurl string        URL of the LORIOT server used for data processing
      --nwkskey string                Nwkskey of the device according to LoRaWAN specification (default "7F0839C044B950180046AEB5A1E57526")
      --program int                   ID of the program used in the device
      --rabbitmqpassword string       Password of the RabbitMQ server used for data exchange (default "guest")
      --rabbitmqport int              Port of the RabbitMQ server used for data exchange (default 5672)
      --rabbitmqserverurl string      URL of the RabbitMQ server used for data exchange (default "localhost")
      --rabbitmqusername string       User Name of the RabbitMQ server used for data exchange (default "guest")
      --registermethod string         Registeration method of the device is LORIOT. Possible values: none,self,other,auto (default "auto")
      --rejoininterval int            How often must the device perform a join again after an unsuccessful join (default 10)
      --title string                  title or name of the device (default "loranna")
      --uplinkconfirmed               Enable/Disable confirmed uplink in the device
      --uplinkinterval int            How often should the device send uplinks. Unit is second (default 5)
      --uplinkpayload string          Value of the User payload (FRMPayload) in every uplink messages (default "0000")
      --uplinkport int                Value of port (FPort) used in every uplink message (default 22)
```

### Options inherited from parent commands

```
      --serveraddress string   URL of the Lorannad server (default "localhost")
      --serverport uint        Port of the Lorannad server (default 35998)
```

### SEE ALSO

* [loranna device](#loranna-device)	 - devices related commands

