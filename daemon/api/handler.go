package api

import (
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/loranna/loranna/daemon/simulation"
)

type Handlers struct {
	logger       *logrus.Logger
	errorChannel chan<- error
	simulation   *simulation.Simulation
}

func (h *Handlers) Logger(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		startTime := time.Now()
		defer h.logger.Printf("request processed in %s\n", time.Since(startTime))
		next(w, r)
	}
}
func (h *Handlers) SetupRoutes(mux *mux.Router) {
	mux.HandleFunc("/broker/{cmd}", h.broker)
	mux.HandleFunc("/environment/{cmd}", h.environment)
	mux.HandleFunc("/gateway/{cmd}", h.gateway)
	mux.HandleFunc("/device/{cmd}", h.device)
}

//NewHandlers creates a new Handlers struct
func NewHandlers(logger *logrus.Logger, errchannel chan<- error) *Handlers {
	h := &Handlers{
		simulation:   simulation.New(logger, errchannel),
		logger:       logger,
		errorChannel: errchannel,
	}
	err := h.simulation.Start()
	if err != nil {
		errchannel <- err
	}
	return h
}

func (h *Handlers) Close() error {
	if h.simulation != nil {
		return h.simulation.Close()
	}
	return nil
}
