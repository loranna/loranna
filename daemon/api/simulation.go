package api

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	"gitlab.com/loranna/loranna/daemon/api/message"
	"gitlab.com/loranna/loranna/daemon/process"
	"gitlab.com/loranna/loranna/networkinterface"
)

func (h *Handlers) broker(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	cmd := vars["cmd"]
	switch cmd {
	case "start":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		err = h.simulation.BrokerStart(string(body))
		if err != nil {
			h.errorhandling(w, err)
			return
		}
	case "stop":
		err := h.simulation.BrokerStop()
		if err != nil {
			h.errorhandling(w, err)
			return
		}
	case "status":
		status, err := h.simulation.BrokerStatus()
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		b, err := message.NewStatus(status.String())
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		w.Write(b)
	case "update":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		v, err := message.UnmarshalVersion(body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		err = h.simulation.BrokerUpdate(v.Version)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		b, err := message.NewStatus("rabbitmq updated")
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		w.Write(b)
	default:
		err := fmt.Errorf("unknown cmd %s", cmd)
		h.errorhandling(w, err)
	}
}

func (h *Handlers) environment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	cmd := vars["cmd"]
	switch cmd {
	case "start":
		v := viper.New()
		v.SetConfigType("json")
		err := v.ReadConfig(r.Body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		defer r.Body.Close()

		err = h.simulation.EnvironmentStart(v)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
	case "stop":
		err := h.simulation.EnvironmentStop()
		if err != nil {
			h.errorhandling(w, err)
			return
		}
	case "status":
		status, err := h.simulation.EnvironmentStatus()
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		b, err := message.NewStatus(status.String())
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		w.Write(b)
	case "update":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		v, err := message.UnmarshalVersion(body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		err = h.simulation.EnvironmentUpdate(v.Version)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		b, err := message.NewStatus("environment updated")
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		w.Write(b)
	case "version":
		v, err := h.simulation.EnvironmentVersion()
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		b, err := message.NewVersion(v)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		w.Write(b)
	default:
		err := fmt.Errorf("unknown cmd %s", cmd)
		h.errorhandling(w, err)
	}
}

func (h *Handlers) device(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	cmd := vars["cmd"]
	switch cmd {
	case "start":
		v := viper.New()
		v.SetConfigType("json")
		err := v.ReadConfig(r.Body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		defer r.Body.Close()
		err = h.simulation.DeviceStart(v)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
	case "stop":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		defer r.Body.Close()
		m, err := message.UnmarshalID(body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		if m.ID < 0 {
			err = h.simulation.DeviceStopAll()
			if err != nil {
				h.errorhandling(w, err)
				return
			}
		} else {
			err = h.simulation.DeviceStop(m.ID)
			if err != nil {
				h.errorhandling(w, err)
				return
			}
		}
	case "status":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		defer r.Body.Close()
		m, err := message.UnmarshalID(body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		if m.ID < 0 {
			statuses, err := h.simulation.DevicesStatus()
			if err != nil {
				h.errorhandling(w, err)
				return
			}
			b, err := message.NewStatuses(statuses)
			if err != nil {
				h.errorhandling(w, err)
				return
			}
			w.Write(b)
		} else {
			status, err := h.simulation.DeviceStatus(m.ID)
			if err != nil {
				h.errorhandling(w, err)
				return
			}
			b, err := message.NewStatus(status.String())
			if err != nil {
				h.errorhandling(w, err)
				return
			}
			w.Write(b)
		}
	case "version":
		v, err := h.simulation.DeviceVersion()
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		b, err := message.NewVersion(v)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		w.Write(b)
	case "update":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		v, err := message.UnmarshalVersion(body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		err = h.simulation.DeviceUpdate(v.Version)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		b, err := message.NewStatus("device updated")
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		w.Write(b)
	default:
		err := fmt.Errorf("unknown cmd %s", cmd)
		h.errorhandling(w, err)
	}
}

func (h *Handlers) gateway(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	cmd := vars["cmd"]
	switch cmd {
	case "info":
		runner := r.FormValue("runner")
		switch runner {
		case "native":
			iname, err := networkinterface.Defaultroutename()
			if err != nil {
				h.logger.Errorf("%v", err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			mac, err := networkinterface.Macaddr(iname)
			if err != nil {
				h.logger.Errorf("%v", err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			b, err := message.NewGatewayInfo(mac, iname)
			fmt.Println(string(b))
			if err != nil {
				h.logger.Errorf("%v", err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			w.Write(b)
		case "docker":
			r := rand.Int()
			mac := fmt.Sprintf("7A:AA:A5:%02X:%02X:%02X", byte(r), byte(r>>8), byte(r>>16))
			b, err := message.NewGatewayInfo(mac, "eth0")
			fmt.Println(string(b))
			if err != nil {
				h.logger.Errorf("%v", err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			w.Write(b)
		default:
			h.logger.Errorf("unknown runner: %s", runner)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	case "start":
		runner := r.FormValue("runner")
		v := viper.New()
		v.SetConfigType("json")
		err := v.ReadConfig(r.Body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		defer r.Body.Close()
		err = h.simulation.GatewayStart(v, runner)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
	case "stop":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		defer r.Body.Close()
		m, err := message.UnmarshalID(body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		if m.ID < 0 {
			err = h.simulation.GatewaysStopAll()
			if err != nil {
				h.errorhandling(w, err)
				return
			}
		} else {
			err = h.simulation.GatewayStop(m.ID)
			if err != nil {
				h.errorhandling(w, err)
				return
			}
		}
	case "status":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		defer r.Body.Close()
		m, err := message.UnmarshalID(body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		if m.ID < 0 {
			statuses, err := h.simulation.GatewaysStatus()
			if err != nil {
				h.errorhandling(w, err)
				return
			}
			b, err := message.NewStatuses(statuses)
			if err != nil {
				h.errorhandling(w, err)
				return
			}
			w.Write(b)
		} else {
			status, err := h.simulation.GatewayStatus(m.ID)
			if err != nil {
				h.errorhandling(w, err)
				return
			}
			b, err := message.NewStatus(status.String())
			if err != nil {
				h.errorhandling(w, err)
				return
			}
			w.Write(b)
		}
	case "update":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		v, err := message.UnmarshalVersion(body)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		err = h.simulation.GatewayUpdate(process.Loriot, v.Version)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		b, err := message.NewStatus("gateway updated")
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		w.Write(b)
	case "version":
		v, err := h.simulation.GatewayVersion()
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		b, err := message.NewVersion(v)
		if err != nil {
			h.errorhandling(w, err)
			return
		}
		w.Write(b)
	default:
		err := fmt.Errorf("unknown cmd %s", cmd)
		h.errorhandling(w, err)
	}
}

func (h *Handlers) errorhandling(w http.ResponseWriter, err error) {
	if err == nil {
		return
	}
	h.logger.Errorf("%v", err)
	b, err := message.NewError(err)
	if err != nil {
		h.logger.Errorf("%v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusBadRequest)
	w.Write(b)
}
