package message

import (
	"encoding/json"

	"gitlab.com/loranna/loranna/daemon/process"
)

type Error struct {
	Error string `json:"error"`
}

func NewError(str error) ([]byte, error) {
	b, err := json.Marshal(Error{Error: str.Error()})
	if err != nil {
		return nil, err
	}
	return b, nil
}

type Status struct {
	Status string `json:"status"`
}

func NewStatus(str string) ([]byte, error) {
	b, err := json.Marshal(Status{Status: str})
	if err != nil {
		return nil, err
	}
	return b, nil
}

func UnmarshalStatus(body []byte) (*Status, error) {
	v := Status{}
	err := json.Unmarshal(body, &v)
	if err != nil {
		return nil, err
	}
	return &v, nil
}

type Statuses struct {
	Statuses []process.Status `json:"statuses"`
}

func NewStatuses(str []process.Status) ([]byte, error) {
	b, err := json.Marshal(Statuses{Statuses: str})
	if err != nil {
		return nil, err
	}
	return b, nil
}

func UnmarshalStatuses(body []byte) ([]process.Status, error) {
	v := Statuses{}
	err := json.Unmarshal(body, &v)
	if err != nil {
		return nil, err
	}
	return v.Statuses, nil
}

type Version struct {
	Version string `json:"version"`
}

func NewVersion(str string) ([]byte, error) {
	b, err := json.Marshal(Version{Version: str})
	if err != nil {
		return nil, err
	}
	return b, nil
}

func UnmarshalVersion(body []byte) (*Version, error) {
	v := Version{}
	err := json.Unmarshal(body, &v)
	if err != nil {
		return nil, err
	}
	return &v, nil
}

type ID struct {
	ID int `json:"id"`
}

func NewID(id int) ([]byte, error) {
	b, err := json.Marshal(ID{ID: id})
	if err != nil {
		return nil, err
	}
	return b, nil
}

func UnmarshalID(body []byte) (*ID, error) {
	v := ID{}
	err := json.Unmarshal(body, &v)
	if err != nil {
		return nil, err
	}
	return &v, nil
}

type GatewayVersion struct {
	GatewayType process.GatewayType `json:"gatewaytype"`
	Version     string              `json:"version"`
}

func NewGatewayVersion(t process.GatewayType, version string) ([]byte, error) {
	b, err := json.Marshal(GatewayVersion{t, version})
	if err != nil {
		return nil, err
	}
	return b, nil
}

func UnmarshalGatewayVersion(body []byte) (*GatewayVersion, error) {
	v := GatewayVersion{}
	err := json.Unmarshal(body, &v)
	if err != nil {
		return nil, err
	}
	return &v, nil
}

type Versions []Version

func NewVersions(str []string) ([]byte, error) {
	b := make([]Version, len(str))
	for i := 0; i < len(b); i++ {
		b[i] = Version{Version: str[i]}
	}
	bb, err := json.Marshal(b)
	if err != nil {
		return nil, err
	}
	return bb, nil
}

func UnmarshalVersions(body []byte) (*Versions, error) {
	v := Versions{}
	err := json.Unmarshal(body, &v)
	if err != nil {
		return nil, err
	}
	return &v, nil
}

type GatewayInfo struct {
	InterfaceName string
	MacAddress    string
}

func NewGatewayInfo(mac, interfacename string) ([]byte, error) {
	b := GatewayInfo{MacAddress: mac, InterfaceName: interfacename}
	bb, err := json.Marshal(b)
	if err != nil {
		return nil, err
	}
	return bb, nil
}

func UnmarshalGatewayInfo(body []byte) (*GatewayInfo, error) {
	v := GatewayInfo{}
	err := json.Unmarshal(body, &v)
	if err != nil {
		return nil, err
	}
	return &v, nil
}
