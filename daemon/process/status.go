package process

//go:generate stringer -type=Status

type Status int

const (
	NotRunning Status = iota
	Running
)
