package broker

import (
	"fmt"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/loranna/loranna/daemon/process"
)

type BrokerProcess struct {
	externallogger *logrus.Logger
	version        string
}

const defaultversion = "3.8.3"

func New(logger *logrus.Logger) *BrokerProcess {
	return &BrokerProcess{
		externallogger: logger,
		version:        defaultversion,
	}
}

func (ep *BrokerProcess) Close() error {
	err := ep.Stop()
	if err != nil {
		return err
	}
	return nil
}

func (ep *BrokerProcess) Setup(conf string) error {
	// rabbitmq started in docker, hardcoded. no need for setup
	return nil
}

func (ep *BrokerProcess) Start() error {
	p := process.New(ep.externallogger)
	_, err := p.Run(fmt.Sprintf("docker run -d --name broker -p 5672:5672 -p 15672:15672 rabbitmq:%s-management", ep.version))
	if err != nil {
		return err
	}
	return nil
}

func (ep *BrokerProcess) Stop() error {
	p := process.New(ep.externallogger)
	_, output, err := p.RunForOutput("docker ps -f name=broker -q")
	if err != nil {
		return err
	}
	id := strings.TrimSpace(output)
	if len(id) == 0 {
		return nil
	} else if len(id) == 12 {
		_, _, err := p.RunForOutput("docker rm -f " + id)
		if err != nil {
			return err
		}
	} else {
		return fmt.Errorf("something went wrong while stopping the broker")
	}
	ep.externallogger.Debug("broker stopped")
	return nil
}

func (ep *BrokerProcess) Status() (process.Status, error) {
	// this should be parametrized once docker hardcoded implementation is dropped
	url := fmt.Sprintf("amqp://guest:guest@localhost:5672")
	c, err := amqp.Dial(url)
	if err != nil {
		return process.NotRunning, err
	}
	c.Close()
	return process.Running, nil
}

func (ep *BrokerProcess) Update(version string) error {
	p := process.New(ep.externallogger)
	_, err := p.Run(fmt.Sprintf("docker pull rabbitmq:%s-management", ep.version))
	if err != nil {
		return err
	}
	ep.version = version
	return nil
}

func (ep *BrokerProcess) Version() (string, error) {
	return strings.TrimSpace(ep.version), nil
}
