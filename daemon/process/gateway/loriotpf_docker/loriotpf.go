package loriotpf_docker

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/loranna/configuration/gateway"
	"gitlab.com/loranna/loranna/daemon/process"
	"gitlab.com/loranna/loranna/daemon/process/gateway/internal/loriot"
	"gitlab.com/loranna/loranna/networkinterface"
)

type LoriotPFDockerProcess struct {
	externallogger *logrus.Logger
	version        string
	dockerID       string
	configuration  *viper.Viper
	id             int
}

var BinName = "virtualv1"
var logPath = path.Join(process.Rootpath, "log/gateway")
var cfgPath = path.Join(process.Rootpath, "config/gateway")
var verFile = path.Join(process.Rootpath, "version")

func New(elogger *logrus.Logger, id int) *LoriotPFDockerProcess {
	return &LoriotPFDockerProcess{
		externallogger: elogger,
		version:        loriot.Defaultversion,
		id:             id,
	}
}

func (lpf *LoriotPFDockerProcess) Close() error {
	err := lpf.Stop()
	if err != nil {
		return err
	}
	return nil
}

func (lpf *LoriotPFDockerProcess) Setup(configuration *viper.Viper) error {
	os.MkdirAll(cfgPath, 0755)
	lpf.configuration = configuration

	defaultip, err := networkinterface.DefaultInterfaceIP()
	if err != nil {
		return err
	}
	lpf.configuration.Set("rabbitmqserverurl", defaultip)
	lpf.configuration.Set("environmentserverurl", defaultip)

	lpf.configuration.WriteConfigAs(path.Join(cfgPath, fmt.Sprintf("config%d.yml", lpf.id)))
	return nil
}

func (lpf *LoriotPFDockerProcess) Start() error {
	c, err := gateway.LoadConfig(lpf.configuration)
	if err != nil {
		return err
	}
	p := process.New(lpf.externallogger)
	_, id, err := p.RunForOutput(fmt.Sprintf("docker run -d --cap-add NET_ADMIN --mac-address %s --name gateway%d --entrypoint stdbuf -v %s:/config.yml registry.gitlab.com/loriot/agent:%s -oL /virtualv1 -f -s %s -E %s",
		c.MacAddress, lpf.id,
		path.Join(cfgPath, fmt.Sprintf("config%d.yml", lpf.id)), lpf.version,
		c.LoriotServerURL, c.NetworkInterfaceName))
	if err != nil {
		return err
	}
	if c.ExpectedLatency > 0 {
		// https://medium.com/@kazushi/simulate-high-latency-network-using-docker-containerand-tc-commands-a3e503ea4307
		_, err = p.Run(fmt.Sprintf("docker exec gateway tc qdisc add dev eth0 root netem delay %dms", int(c.ExpectedLatency)))
		if err != nil {
			return err
		}
	}
	lpf.dockerID = strings.TrimSpace(id)
	return nil
}

func (lpf *LoriotPFDockerProcess) Stop() error {
	if len(lpf.dockerID) > 0 {
		p := process.New(lpf.externallogger)
		_, err := p.Run(fmt.Sprintf("docker rm -f gateway%d", lpf.id))
		if err != nil {
			return err
		}
		lpf.externallogger.Debugf("gateway process stopped")
	}
	return nil
}

func (lpf *LoriotPFDockerProcess) Status() (process.Status, error) {
	if len(lpf.dockerID) == 0 {
		return process.NotRunning, nil
	}
	if lpf.containerstopped() {
		return process.NotRunning, nil
	}
	return process.Running, nil
}

func (lpf *LoriotPFDockerProcess) Update(version string) error {
	if len(version) != 0 {
		lpf.version = version
	}
	err := os.MkdirAll(process.BinPath, 0755)
	if err != nil {
		return err
	}
	p := process.New(lpf.externallogger)
	_, err = p.Run("docker pull registry.gitlab.com/loriot/agent:" + lpf.version)
	if err != nil {
		return err
	}
	_, err = p.Run("docker run -d --entrypoint tail --name temp_gw registry.gitlab.com/loriot/agent:" + lpf.version + " -f /dev/null")
	if err != nil {
		return err
	}
	_, err = p.Run("docker cp temp_gw:/virtualv1 " + path.Join(process.BinPath, BinName))
	if err != nil {
		return err
	}
	_, err = p.Run("docker rm -f temp_gw")
	if err != nil {
		return err
	}
	f, err := os.Create(verFile)
	if err != nil {
		return err
	}
	defer f.Close()
	_, err = f.WriteString(lpf.version)
	if err != nil {
		return err
	}
	return nil
}

func (lpf *LoriotPFDockerProcess) Version() (string, error) {
	output, err := ioutil.ReadFile(verFile)
	if err != nil {
		return "", err
	}
	return strings.TrimSpace(string(output)), nil
}

func (lpf *LoriotPFDockerProcess) containerstopped() bool {
	p := process.New(lpf.externallogger)
	_, r, err := p.RunForOutput(fmt.Sprintf("docker ps --filter id=%s", lpf.dockerID))
	if err != nil {
		return false
	}
	numoflines := strings.Split(r, "\n")
	if len(numoflines) > 1 {
		return false
	}
	return true
}
