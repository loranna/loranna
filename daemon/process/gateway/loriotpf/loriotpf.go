package loriotpf

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/loranna/configuration/gateway"
	"gitlab.com/loranna/loranna/daemon/process"
	"gitlab.com/loranna/loranna/daemon/process/gateway/internal/loriot"
)

type LoriotPFProcess struct {
	externallogger *logrus.Logger
	logger         *logrus.Logger
	cmd            *exec.Cmd
	version        string
	configuration  *viper.Viper
}

var BinName = "virtualv1"
var logPath = path.Join(process.Rootpath, "log/gateway")
var cfgPath = path.Join(process.Rootpath, "config/gateway")
var verFile = path.Join(process.Rootpath, "version")

func New(elogger *logrus.Logger) *LoriotPFProcess {
	logger := logrus.New()
	os.MkdirAll(logPath, 0755)
	filename := path.Join(logPath, "log")
	f, err := os.Create(filename)
	if err != nil {
		logrus.Errorf("could not create log file %s. error: %v", filename, err)
	}
	logger.Out = f
	return &LoriotPFProcess{
		logger:         logger,
		externallogger: elogger,
		version:        loriot.Defaultversion,
	}
}

func (lpf *LoriotPFProcess) Close() error {
	err := lpf.Stop()
	if err != nil {
		return err
	}
	return nil
}

func (lpf *LoriotPFProcess) Setup(configuration *viper.Viper) error {
	os.MkdirAll(cfgPath, 0755)
	lpf.configuration = configuration
	lpf.configuration.WriteConfigAs(path.Join(cfgPath, "config.yml"))
	return nil
}

func (lpf *LoriotPFProcess) Start() error {
	c, err := gateway.LoadConfig(lpf.configuration)
	if err != nil {
		return err
	}
	command := fmt.Sprintf("stdbuf -oL %s -f -s %s -E %s",
		path.Join(process.BinPath, BinName),
		c.LoriotServerURL, c.NetworkInterfaceName)
	lpf.logger.Debug(command)
	parts := strings.Split(command, " ")
	cmd := exec.Command(parts[0], parts[1:]...)
	cmd.Dir = cfgPath
	cmd.Stdout = lpf.logger.Out
	cmd.Stderr = lpf.logger.Out
	cmd.Env = append(cmd.Env, gateway.Env(lpf.configuration)...)
	err = cmd.Start()
	if err != nil {
		return err
	}
	go wait(lpf.logger, cmd)
	lpf.cmd = cmd
	return nil
}

func (lpf *LoriotPFProcess) Stop() error {
	if lpf.cmd != nil && lpf.cmd.Process != nil {
		err := lpf.cmd.Process.Signal(os.Interrupt)
		if err != nil {
			return err
		}
		lpf.externallogger.Debugf("gateway process stopped")
	}
	return nil
}

func (lpf *LoriotPFProcess) Status() (process.Status, error) {
	if lpf.cmd == nil {
		return process.NotRunning, nil
	}
	if lpf.cmd.ProcessState != nil && lpf.cmd.ProcessState.Exited() {
		return process.NotRunning, nil
	}
	return process.Running, nil
}

func (lpf *LoriotPFProcess) Update(version string) error {
	if len(version) != 0 {
		lpf.version = version
	}
	err := os.MkdirAll(process.BinPath, 0755)
	if err != nil {
		return err
	}
	p := process.New(lpf.externallogger)
	_, err = p.Run("docker pull registry.gitlab.com/loriot/agent:" + lpf.version)
	if err != nil {
		return err
	}
	_, err = p.Run("docker run -d --entrypoint tail --name temp_gw registry.gitlab.com/loriot/agent:" + lpf.version + " -f /dev/null")
	if err != nil {
		return err
	}
	_, err = p.Run("docker cp temp_gw:/virtualv1 " + path.Join(process.BinPath, BinName))
	if err != nil {
		return err
	}
	_, err = p.Run("docker rm -f temp_gw")
	if err != nil {
		return err
	}
	f, err := os.Create(verFile)
	if err != nil {
		return err
	}
	defer f.Close()
	_, err = f.WriteString(lpf.version)
	if err != nil {
		return err
	}
	return nil
}

func (lpf *LoriotPFProcess) Version() (string, error) {
	output, err := ioutil.ReadFile(verFile)
	if err != nil {
		return "", err
	}
	return strings.TrimSpace(string(output)), nil
}

func wait(logger *logrus.Logger, cmd *exec.Cmd) {
	err := cmd.Wait()
	if err != nil {
		logger.Debugf("gateway process completed with error %v", err)
		return
	}
	logger.Debugf("gateway process completed")
}
