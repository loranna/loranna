package gateway

import (
	"github.com/spf13/viper"
	"gitlab.com/loranna/loranna/daemon/process"
)

type Process interface {
	Setup(configuration *viper.Viper) error
	Start() error
	Stop() error
	Status() (process.Status, error)
	Update(version string) error
	Version() (string, error)
	Close() error
}
