package environment

import (
	"os"
	"os/exec"
	"path"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/loranna/configuration/environment"
	"gitlab.com/loranna/loranna/daemon/process"
)

type EnvironmentProcess struct {
	externallogger *logrus.Logger
	logger         *logrus.Logger
	cmd            *exec.Cmd
	version        string
	configuration  *viper.Viper
}

var BinName = "venvironment"
var logPath = path.Join(process.Rootpath, "log/environment")
var cfgPath = path.Join(process.Rootpath, "config/environment")

const defaultversion = "0.12.169"

func New(elogger *logrus.Logger) *EnvironmentProcess {
	l := logrus.New()
	os.MkdirAll(logPath, 0755)
	filename := path.Join(logPath, "log")
	f, err := os.Create(filename)
	if err != nil {
		elogger.Warningf("could not create log file %s. error: %v", filename, err)
	} else {
		l.Out = f
	}
	return &EnvironmentProcess{
		logger:         l,
		externallogger: elogger,
		version:        defaultversion,
	}
}

func (ep *EnvironmentProcess) Close() error {
	err := ep.Stop()
	if err != nil {
		ep.externallogger.Warningf("could not stop environment: %v", err)
	}
	return nil
}

func (ep *EnvironmentProcess) Setup(v *viper.Viper) error {
	os.MkdirAll(cfgPath, 0755)
	ep.configuration = v
	v.WriteConfigAs(path.Join(cfgPath, "config.yml"))
	return nil
}

func (ep *EnvironmentProcess) Start() error {
	command := path.Join(process.BinPath, BinName) + " start"
	ep.externallogger.Debug(command)
	parts := strings.Split(command, " ")
	cmd := exec.Command(parts[0], parts[1:]...)
	cmd.Dir = cfgPath
	cmd.Stdout = ep.logger.Out
	cmd.Stderr = ep.logger.Out
	cmd.Env = append(cmd.Env, environment.Env(ep.configuration)...)
	err := cmd.Start()
	if err != nil {
		return err
	}
	go wait(ep.externallogger, cmd)
	ep.cmd = cmd
	return nil
}

func (ep *EnvironmentProcess) Stop() error {
	if ep.cmd != nil && ep.cmd.Process != nil {
		err := ep.cmd.Process.Signal(os.Interrupt)
		if err != nil {
			return err
		}
		ep.logger.Debug("environment process stopped")
	}
	return nil
}

func (ep *EnvironmentProcess) Status() (process.Status, error) {
	if ep.cmd == nil {
		return process.NotRunning, nil
	}
	if ep.cmd.ProcessState != nil && ep.cmd.ProcessState.Exited() {
		return process.NotRunning, nil
	}
	return process.Running, nil
}

func (ep *EnvironmentProcess) Update(version string) error {
	if len(version) != 0 {
		ep.version = version
	}
	err := os.MkdirAll(process.BinPath, 0755)
	if err != nil {
		return err
	}
	p := process.New(ep.logger)
	_, err = p.Run("docker pull registry.gitlab.com/loranna/venvironment:" + ep.version)
	if err != nil {
		return err
	}
	_, err = p.Run("docker run -d --entrypoint tail --name temp_env registry.gitlab.com/loranna/venvironment:" + ep.version + " -f /dev/null")
	if err != nil {
		return err
	}
	_, err = p.Run("docker cp temp_env:/venvironment " + path.Join(process.BinPath, BinName))
	if err != nil {
		return err
	}
	_, err = p.Run("docker rm -f temp_env")
	if err != nil {
		return err
	}
	return nil
}

func (ep *EnvironmentProcess) Version() (string, error) {
	p := process.New(ep.logger)
	_, output, err := p.RunInDirForOutput("./"+BinName+" version", process.BinPath)
	if err != nil {
		return "", err
	}
	return strings.TrimSpace(output), nil
}

func wait(logger *logrus.Logger, cmd *exec.Cmd) {
	err := cmd.Wait()
	if err != nil {
		logger.Debugf("environment process completed with error %v", err)
		return
	}
	logger.Debug("environment process completed")
}
