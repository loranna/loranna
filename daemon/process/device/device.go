package device

import (
	"os"
	"os/exec"
	"path"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/loranna/configuration/device"
	"gitlab.com/loranna/loranna/daemon/process"
)

type DeviceProcess struct {
	externallogger *logrus.Logger
	logger         *logrus.Logger
	cmd            *exec.Cmd
	version        string
	configuration  *viper.Viper
}

var BinName = "vdevice"
var logPathbase = path.Join(process.Rootpath, "log/devices")
var cfgPathbase = path.Join(process.Rootpath, "config/devices")

const defaultversion = "0.12.245"

func New(elogger *logrus.Logger, id int) *DeviceProcess {
	logger := logrus.New()
	logPath := path.Join(logPathbase, strconv.Itoa(id))
	os.MkdirAll(logPath, 0755)
	filename := path.Join(logPath, "log")
	f, err := os.Create(filename)
	if err != nil {
		logrus.Errorf("could not create log file %s. error: %v", filename, err)
		return nil
	}
	logger.Out = f
	return &DeviceProcess{
		logger:         logger,
		externallogger: elogger,
		version:        defaultversion,
	}
}

func (dev *DeviceProcess) Close() error {
	err := dev.Stop()
	if err != nil {
		return err
	}
	return nil
}

func (dev *DeviceProcess) Setup(c *viper.Viper, id int) error {
	cfgPath := path.Join(cfgPathbase, strconv.Itoa(id))
	os.MkdirAll(cfgPath, 0755)
	dev.configuration = c
	return nil
}

func (dev *DeviceProcess) Start(id int) error {
	command := path.Join(process.BinPath, BinName) + " start"
	dev.externallogger.Debug(command)
	parts := strings.Split(command, " ")
	cmd := exec.Command(parts[0], parts[1:]...)
	cmd.Dir = path.Join(cfgPathbase, strconv.Itoa(id))
	cmd.Stdout = dev.logger.Out
	cmd.Stderr = dev.logger.Out
	cmd.Env = append(cmd.Env, device.Env(dev.configuration)...)
	err := cmd.Start()
	if err != nil {
		return err
	}
	go wait(dev.logger, cmd, id)
	dev.cmd = cmd
	return nil
}

func (dev *DeviceProcess) Stop() error {
	if dev.cmd != nil && dev.cmd.Process != nil {
		err := dev.cmd.Process.Signal(os.Interrupt)
		if err != nil {
			return err
		}
		dev.logger.Debugf("device process stopped")
	}
	return nil
}

func (dev *DeviceProcess) Status() (process.Status, error) {
	if dev.cmd == nil {
		return process.NotRunning, nil
	}
	if dev.cmd.ProcessState != nil && dev.cmd.ProcessState.Exited() {
		return process.NotRunning, nil
	}
	return process.Running, nil
}

func (dev *DeviceProcess) Update(version string) error {
	if len(version) != 0 {
		dev.version = version
	}
	err := os.MkdirAll(process.BinPath, 0755)
	if err != nil {
		return err
	}
	p := process.New(dev.logger)
	_, err = p.Run("docker pull registry.gitlab.com/loranna/vdevice:" + dev.version)
	if err != nil {
		return err
	}
	_, err = p.Run("docker run -d --entrypoint tail --name temp_dev registry.gitlab.com/loranna/vdevice:" + dev.version + " -f /dev/null")
	if err != nil {
		return err
	}
	_, err = p.Run("docker cp temp_dev:/vdevice " + path.Join(process.BinPath, BinName))
	if err != nil {
		return err
	}
	_, err = p.Run("docker rm -f temp_dev")
	if err != nil {
		return err
	}
	return nil
}

func (dev *DeviceProcess) Version() (string, error) {
	p := process.New(dev.logger)
	_, output, err := p.RunInDirForOutput("./"+BinName+" version", process.BinPath)
	if err != nil {
		return "", err
	}
	return strings.TrimSpace(output), nil
}

func wait(logger *logrus.Logger, cmd *exec.Cmd, id int) {
	err := cmd.Wait()
	if err != nil {
		logger.Debugf("device process #%d completed with error %v", id, err)
		return
	}
	logger.Debugf("device process #%d completed", id)
}
