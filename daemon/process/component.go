package process

//go:generate stringer -type=GatewayType

type GatewayType int

const (
	Loriot GatewayType = iota
)

//go:generate stringer -type=DeviceType

type DeviceType int

const (
	Loranna DeviceType = iota
)
