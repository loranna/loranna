package process

import (
	"os/exec"
	"path"
	"strings"

	"github.com/sirupsen/logrus"
)

const Rootpath = "/var/lib/lorannad"

var BinPath = path.Join(Rootpath, "bin")

type Process struct {
	logger *logrus.Logger
}

func New(logger *logrus.Logger) *Process {
	if logger == nil {
		return &Process{logger: logrus.New()}
	}
	return &Process{logger: logger}
}

func (p *Process) Run(command string) (*exec.Cmd, error) {
	p.logger.Debug(command)
	parts := strings.Split(command, " ")
	cmd := exec.Command(parts[0], parts[1:]...)
	cmd.Stdout = p.logger.Out
	cmd.Stderr = p.logger.Out
	err := cmd.Run()
	if err != nil {
		return nil, err
	}
	return cmd, nil
}

func (p *Process) Start(command, dir string) (*exec.Cmd, error) {
	p.logger.Debug(command)
	parts := strings.Split(command, " ")
	cmd := exec.Command(parts[0], parts[1:]...)
	cmd.Dir = dir
	cmd.Stdout = p.logger.Out
	cmd.Stderr = p.logger.Out
	err := cmd.Start()
	if err != nil {
		return nil, err
	}
	return cmd, nil
}

func (p *Process) RunInDirForOutput(command, dir string) (*exec.Cmd, string, error) {
	p.logger.Debug(command)
	parts := strings.Split(command, " ")
	cmd := exec.Command(parts[0], parts[1:]...)
	cmd.Dir = dir
	cmd.Stderr = p.logger.Out
	bytes, err := cmd.Output()
	if err != nil {
		return nil, "", err
	}
	return cmd, string(bytes), nil
}

func (p *Process) RunForOutput(command string) (*exec.Cmd, string, error) {
	p.logger.Debug(command)
	parts := strings.Split(command, " ")
	cmd := exec.Command(parts[0], parts[1:]...)
	cmd.Stderr = p.logger.Out
	bytes, err := cmd.Output()
	if err != nil {
		return nil, "", err
	}
	return cmd, string(bytes), nil
}
