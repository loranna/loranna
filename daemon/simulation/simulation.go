package simulation

import (
	"errors"
	"fmt"
	"math"
	"sync"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/loranna/loranna/daemon/process"
	"gitlab.com/loranna/loranna/daemon/process/broker"
	"gitlab.com/loranna/loranna/daemon/process/device"
	"gitlab.com/loranna/loranna/daemon/process/environment"
	"gitlab.com/loranna/loranna/daemon/process/gateway"
	"gitlab.com/loranna/loranna/daemon/process/gateway/loriotpf"
	"gitlab.com/loranna/loranna/daemon/process/gateway/loriotpf_docker"
)

const devicestartingport = 36000

type Simulation struct {
	logger       *logrus.Logger
	errorChannel chan<- error
	broker       *broker.BrokerProcess
	sync.Mutex
	environment *environment.EnvironmentProcess
	gateways    []gateway.Process
	devices     []*device.DeviceProcess
}

func New(logger *logrus.Logger, errorChannel chan<- error) *Simulation {
	return &Simulation{
		logger:       logger,
		errorChannel: errorChannel,
	}
}

func (s *Simulation) Start() error {
	s.logger.Infoln("broker started with default configuration")
	// rabbitmq started in docker, hardcoded. no need for setup
	return s.BrokerStart("")
}

func (s *Simulation) BrokerStart(conf string) error {
	br := broker.New(s.logger)
	err := br.Setup(conf)
	if err != nil {
		return err
	}
	err = br.Start()
	if err != nil {
		return err
	}
	s.broker = br
	return nil
}

func (s *Simulation) BrokerStop() error {
	if s.broker != nil {
		err := s.broker.Stop()
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *Simulation) BrokerStatus() (process.Status, error) {
	if s.broker == nil {
		return process.NotRunning, fmt.Errorf("broker process is not running")
	}
	return s.broker.Status()
}

func (s *Simulation) BrokerUpdate(version string) error {
	e := broker.New(s.logger)
	err := e.Update(version)
	if err != nil {
		return err
	}
	return nil
}

func (s *Simulation) BrokerVersion() (string, error) {
	if s.environment != nil {
		return s.environment.Version()
	}
	return "", fmt.Errorf("environment is not running")
}

func (s *Simulation) EnvironmentStart(v *viper.Viper) error {
	ep := environment.New(s.logger)
	err := ep.Setup(v)
	if err != nil {
		return err
	}
	err = ep.Start()
	if err != nil {
		return err
	}
	s.environment = ep
	return nil
}

func (s *Simulation) EnvironmentStop() error {
	if s.environment != nil {
		err := s.environment.Stop()
		if err != nil {
			return err
		}
		s.environment = nil
	}
	return nil
}

func (s *Simulation) EnvironmentStatus() (process.Status, error) {
	if s.environment == nil {
		return process.NotRunning, fmt.Errorf("environment process is not running")
	}
	return s.environment.Status()
}

// EnvironmentUpdate updates the executable on the daemon
// if version is empty, it will use the default
func (s *Simulation) EnvironmentUpdate(version string) error {
	e := environment.New(s.logger)
	err := e.Update(version)
	if err != nil {
		return err
	}
	return nil
}

func (s *Simulation) EnvironmentVersion() (string, error) {
	if s.environment != nil {
		return s.environment.Version()
	}
	return "", fmt.Errorf("environment is not running")
}

func (s *Simulation) Close() error {
	err := s.BrokerStop()
	if err != nil {
		s.logger.Warningf("could not stop broker: %v", err)
	}
	err = s.EnvironmentStop()
	if err != nil {
		s.logger.Warningf("could not stop environment: %v", err)
	}
	err = s.GatewaysStopAll()
	if err != nil {
		s.logger.Warningf("could not stop gateway: %v", err)
	}
	return nil
}

func (s *Simulation) GatewayStart(configuration *viper.Viper, runner string) error {
	s.logger.Debugln("gateway starting")
	switch runner {
	case "native":
		if len(s.gateways) > 0 {
			return errors.New("native runner cannot handle more than one gateway")
		}
		lpf := loriotpf.New(s.logger)
		err := lpf.Setup(configuration)
		if err != nil {
			return err
		}
		err = lpf.Start()
		if err != nil {
			return err
		}
		s.gateways = append(s.gateways, lpf)
	case "docker":
		s.Lock()
		defer s.Unlock()
		nextid := len(s.gateways)
		lpf := loriotpf_docker.New(s.logger,nextid)
		err := lpf.Setup(configuration)
		if err != nil {
			return err
		}
		err = lpf.Start()
		if err != nil {
			return err
		}
		s.gateways = append(s.gateways, lpf)
	default:
		return fmt.Errorf("unknown runner: %s", runner)
	}

	return nil
}

func (s *Simulation) GatewaysStopAll() error {
	s.Lock()
	defer s.Unlock()
	for i := 0; i < len(s.gateways); i++ {
		err := s.gateways[i].Stop()
		if err != nil {
			s.logger.Warnf("could not stop device #%d: %v", i, err)
		}
	}
	s.gateways = nil
	return nil
}


func (s *Simulation) GatewayStop(id int) error {
	if s.gateways != nil && s.gateways[id] != nil{
		s.logger.Debugln("gateway exist, stopping")
		err := s.gateways[id].Stop()
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *Simulation) GatewayStatus(id int) (process.Status, error) {
	if s.gateways == nil || s.gateways[id] == nil{
		return process.NotRunning, fmt.Errorf("gateway process is not running")
	}
	return s.gateways[id].Status()
}

func (s *Simulation) GatewaysStatus() ([]process.Status, error) {
	statuses := make([]process.Status, len(s.gateways))
	for i := 0; i < len(s.gateways); i++ {
		status, err := s.GatewayStatus(i)
		if err != nil {
			s.logger.Warnf("status call of gateway #%d run into error: %v", i, err)
			statuses[i] = process.NotRunning
		}
		statuses[i] = status
	}
	return statuses, nil
}

func (s *Simulation) GatewayVersion() (string, error) {
	if s.gateways != nil && s.gateways[0] != nil{
		return s.gateways[0].Version()
	}
	return "", fmt.Errorf("gateway is not running")
}

func (s *Simulation) GatewayUpdate(t process.GatewayType, version string) error {
	switch t {
	case process.Loriot:
		lpf := loriotpf.New(s.logger)
		err := lpf.Update(version)
		if err != nil {
			return err
		}
		lpf.Close()
	default:
		return fmt.Errorf("unknown gateway type: %v", t)
	}
	return nil
}

func (s *Simulation) DeviceStart(configuration *viper.Viper) error {
	s.Lock()
	defer s.Unlock()
	nextid := len(s.devices)
	dev := device.New(s.logger, nextid)
	configuration.Set("deviceserverport", devicestartingport+nextid)
	err := dev.Setup(configuration, nextid)

	if err != nil {
		return err
	}
	err = dev.Start(nextid)
	if err != nil {
		return err
	}
	s.devices = append(s.devices, dev)
	return nil
}

func (s *Simulation) DeviceStopAll() error {
	s.Lock()
	defer s.Unlock()
	for i := 0; i < len(s.devices); i++ {
		err := s.devices[i].Stop()
		if err != nil {
			s.logger.Warnf("could not stop device #%d: %v", i, err)
		}
	}
	s.devices = nil
	return nil
}

func (s *Simulation) DeviceStop(id int) error {
	err := s.devices[id].Stop()
	if err != nil {
		s.logger.Warnf("could not stop device #%d: %v", id, err)
	}
	return nil
}

func (s *Simulation) DevicesStatus() ([]process.Status, error) {
	statuses := make([]process.Status, len(s.devices))
	for i := 0; i < len(s.devices); i++ {
		status, err := s.DeviceStatus(i)
		if err != nil {
			s.logger.Warnf("status call of device #%d run into error: %v", i, err)
			statuses[i] = process.NotRunning
		}
		statuses[i] = status
	}
	return statuses, nil
}

func (s *Simulation) DeviceStatus(id int) (process.Status, error) {
	return s.devices[id].Status()
}

func (s *Simulation) DeviceVersion() (string, error) {
	if s.devices[0] != nil {
		return s.devices[0].Version()
	}
	return "", fmt.Errorf("no device is running")
}

func (s *Simulation) DeviceUpdate(version string) error {
	d := device.New(s.logger, math.MaxInt64)
	err := d.Update(version)
	if err != nil {
		return err
	}
	d.Close()
	return nil
}
