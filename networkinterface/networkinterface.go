package networkinterface

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strings"
)

func Defaultroutename() (string, error) {
	file, err := os.Open("/proc/net/route")
	if err != nil {
		return "", fmt.Errorf("WARNING, i rely on /proc/net/route, actual error %w", err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		tokens := strings.Split(scanner.Text(), "\t")
		if tokens[1] == "00000000" {
			// default route
			return tokens[0], nil
		}
	}
	return "", fmt.Errorf("could not find default route's name")
}

func Macaddr(ifname string) (string, error) {
	interfaces, err := net.Interfaces()
	if err != nil {
		return "", err
	}
	for _, i := range interfaces {
		if i.Name == strings.TrimSpace(ifname) {
			return i.HardwareAddr.String(), nil
		}
	}
	return "", fmt.Errorf("could not find hwaddr for %s", ifname)
}

func InterfaceName(mac string) (string, error) {
	interfaces, err := net.Interfaces()
	if err != nil {
		return "", err
	}
	for _, i := range interfaces {
		if i.HardwareAddr.String() == strings.TrimSpace(mac) {
			return i.Name, nil
		}
	}
	return "", fmt.Errorf("could not find interfacename for %s", mac)
}

func DefaultInterfaceIP() (string, error) {
	name, err := Defaultroutename()
	if err != nil {
		return "", err
	}
	interf, err := net.InterfaceByName(name)
	if err != nil {
		return "", err
	}
	addrs, _ := interf.Addrs()
	for i := 0; i < len(addrs); i++ {
		ip, _, _ := net.ParseCIDR(addrs[i].String())

		if ip.To4() != nil {
			return ip.String(), nil
		}
	}
	return "", fmt.Errorf("no IPV4 address available on interface %s", name)
}
