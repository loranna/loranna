package cmd

import (
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(brokerCmd)
}

var brokerCmd = &cobra.Command{
	Use:    "broker",
	Short:  "broker related commands",
	Hidden: true,
}
