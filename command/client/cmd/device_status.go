package cmd

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/spf13/cobra"
	"gitlab.com/loranna/loranna/daemon/api/message"
)

func init() {
	deviceCmd.AddCommand(deviceStatusCmd)
}

var deviceStatusCmd = &cobra.Command{
	Use:   "status [id]",
	Short: "Queries the status of device(s).",
	Long:  `Queries all devices' status, when no parameter supplied
and returns one device's status when id is utilized`,
RunE: func(cmd *cobra.Command, args []string) error {
		id := 0
		if len(args) < 1 {
			id = -1
		}
		c, err := message.NewID(id)
		if err != nil {
			return err
		}
		reader := bytes.NewReader(c)
		daemonurl := fmt.Sprintf("http://%s:%d/device/status", rootViper.GetString("serveraddress"), rootViper.GetUint("serverport"))
		r, err := http.Post(daemonurl, "application/json", reader)
		if err != nil {
			return err
		}
		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			return err
		}
		r.Body.Close()
		fmt.Print(string(b))
		return nil
	},
}
