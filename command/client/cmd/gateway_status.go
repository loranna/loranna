package cmd

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/spf13/cobra"
	"gitlab.com/loranna/loranna/daemon/api/message"
)

func init() {
	gatewayCmd.AddCommand(gatewayStatusCmd)
}

var gatewayStatusCmd = &cobra.Command{
	Use:    "status",
	Short:  "Queries the status of the gateway",
	RunE: func(cmd *cobra.Command, args []string) error {
		id := 0
		if len(args) < 1 {
			id = -1
		}
		c, err := message.NewID(id)
		if err != nil {
			return err
		}
		reader := bytes.NewReader(c)
		daemonurl := fmt.Sprintf("http://%s:%d/gateway/status", rootViper.GetString("serveraddress"), rootViper.GetUint("serverport"))
		r, err := http.Post(daemonurl, "application/json", reader)
		if err != nil {
			return err
		}
		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			return err
		}
		r.Body.Close()
		fmt.Print(string(b))
		return nil
	},
}
