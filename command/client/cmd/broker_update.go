package cmd

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/spf13/cobra"
	"gitlab.com/loranna/configuration/broker"
	"gitlab.com/loranna/loranna/daemon/api/message"
)

func init() {
	broker.AddAndBindFlags(brokerUpdateCmd, brokerViper)
	broker.BindEnv(brokerViper)
	brokerCmd.AddCommand(brokerUpdateCmd)
}

var brokerUpdateCmd = &cobra.Command{
	Use:   "update version",
	Short: "Updates the broker",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("version parameter is mandatory")
		}
		body, err := message.NewVersion(args[0])
		if err != nil {
			return err
		}
		reader := bytes.NewReader(body)
		url := fmt.Sprintf("http://%s:%d/broker/update", rootViper.GetString("serveraddress"), rootViper.GetUint("serverport"))
		r, err := http.Post(url, "application/json", reader)
		if err != nil {
			return err
		}

		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			return err
		}
		fmt.Print(string(b))
		return nil
	},
}
