package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/loranna/configuration/environment"
)

var envViper = viper.New()

func init() {
	environment.AddAndBindFlags(environmentStartCmd, envViper)
	environment.BindEnv(envViper)
	environmentCmd.AddCommand(environmentStartCmd)
}

var environmentStartCmd = &cobra.Command{
	Use:   "start",
	Short: "Starts the environment",
	RunE: func(cmd *cobra.Command, args []string) error {
		c, err := environment.LoadConfig(envViper)
		if err != nil {
			return err
		}
		conf, err := json.Marshal(c)
		if err != nil {
			return err
		}
		reader := bytes.NewReader(conf)
		url := fmt.Sprintf("http://%s:%d/environment/start", rootViper.GetString("serveraddress"), rootViper.GetUint("serverport"))
		r, err := http.Post(url, "application/json", reader)
		if err != nil {
			return err
		}
		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			return err
		}
		fmt.Print(string(b))
		return nil
	},
}
