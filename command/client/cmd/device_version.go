package cmd

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/spf13/cobra"
)

func init() {
	deviceCmd.AddCommand(deviceVersionCmd)
}

var deviceVersionCmd = &cobra.Command{
	Use:   "version",
	Short: "Queries the version of the device",
	RunE: func(cmd *cobra.Command, args []string) error {
		daemonurl := fmt.Sprintf("http://%s:%d/device/version", rootViper.GetString("serveraddress"), rootViper.GetUint("serverport"))
		r, err := http.Get(daemonurl)
		if err != nil {
			return err
		}
		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			return err
		}
		r.Body.Close()
		fmt.Print(string(b))
		return nil
	},
}
