package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var runner string
var gatewayLocal = viper.New()

func init() {
	gatewayCmd.PersistentFlags().StringVar(&runner, "runner", "native", "Sets the runner to be used for the gateway. Possible values: native,docker, WARNING: Native can support only one gateway")
	gatewayLocal.BindPFlag("runner", gatewayCmd.PersistentFlags().Lookup("runner"))
	gatewayLocal.BindEnv("runner", "RUNNER")
	rootCmd.AddCommand(gatewayCmd)
}

var gatewayCmd = &cobra.Command{
	Use:   "gateway",
	Short: "gateway related commands",
}
