package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/loranna/configuration/broker"
)

var brokerViper = viper.New()

func init() {
	broker.AddAndBindFlags(brokerStartCmd, brokerViper)
	broker.BindEnv(brokerViper)
	brokerCmd.AddCommand(brokerStartCmd)
}

var brokerStartCmd = &cobra.Command{
	Use:   "start",
	Short: "Starts broker in the server",
	RunE: func(cmd *cobra.Command, args []string) error {
		c, err := broker.LoadConfig(brokerViper)
		if err != nil {
			return err
		}
		conf, err := json.Marshal(c)
		if err != nil {
			return err
		}
		reader := bytes.NewReader(conf)
		url := fmt.Sprintf("http://%s:%d/broker/start", rootViper.GetString("serveraddress"), rootViper.GetUint("serverport"))
		r, err := http.Post(url, "application/json", reader)
		if err != nil {
			return err
		}

		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			return err
		}
		fmt.Print(string(b))
		return nil
	},
}
