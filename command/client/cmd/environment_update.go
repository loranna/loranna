package cmd

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/spf13/cobra"
	"gitlab.com/loranna/loranna/daemon/api/message"
)

func init() {
	environmentCmd.AddCommand(environmentUpdateCmd)
}

var environmentUpdateCmd = &cobra.Command{
	Use:   "update [version]",
	Short: "Updates the environment component",
	RunE: func(cmd *cobra.Command, args []string) error {
		url := fmt.Sprintf("http://%s:%d/environment/update", rootViper.GetString("serveraddress"), rootViper.GetUint("serverport"))
		v, err := message.NewVersion("")
		if err != nil {
			return err
		}
		if len(args) > 0 {
			v, err = message.NewVersion(args[0])
			if err != nil {
				return err
			}
		}

		reader := bytes.NewReader(v)
		r, err := http.Post(url, "application/json", reader)
		if err != nil {
			return err
		}
		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			return err
		}
		fmt.Print(string(b))
		return nil
	},
}
