package cmd

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/spf13/cobra"
)

func init() {
	brokerCmd.AddCommand(brokerStatusCmd)
}

var brokerStatusCmd = &cobra.Command{
	Use:   "status",
	Short: "Queries the status of the broker",
	RunE: func(cmd *cobra.Command, args []string) error {
		url := fmt.Sprintf("http://%s:%d/broker/status", rootViper.GetString("serveraddress"), rootViper.GetUint("serverport"))
		r, err := http.Get(url)
		if err != nil {
			return err
		}

		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			return err
		}
		fmt.Print(string(b))
		return nil
	},
}
