package cmd

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/spf13/cobra"
	"gitlab.com/loranna/loranna/daemon/api/message"
)

func init() {
	environmentCmd.AddCommand(environmentVersionCmd)
}

var environmentVersionCmd = &cobra.Command{
	Use:   "version",
	Short: "Queries the version of the environment",
	RunE: func(cmd *cobra.Command, args []string) error {
		url := fmt.Sprintf("http://%s:%d/environment/version", rootViper.GetString("serveraddress"), rootViper.GetUint("serverport"))
		r, err := http.Get(url)
		if err != nil {
			return err
		}
		responsebody, err := ioutil.ReadAll(r.Body)
		if err != nil {
			return err
		}
		defer r.Body.Close()
		v, err := message.UnmarshalVersion(responsebody)
		if err != nil {
			return err
		}
		fmt.Printf("version of the gateway: %s\n", v.Version)
		return nil
	},
}
