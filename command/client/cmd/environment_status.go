package cmd

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/spf13/cobra"
)

func init() {
	environmentCmd.AddCommand(environmentStatusCmd)
}

var environmentStatusCmd = &cobra.Command{
	Use:    "status",
	Short:  "Queries the status of the environment",
	Hidden: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		url := fmt.Sprintf("http://%s:%d/environment/status", rootViper.GetString("serveraddress"), rootViper.GetUint("serverport"))
		r, err := http.Get(url)
		if err != nil {
			return err
		}

		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			return err
		}
		fmt.Print(string(b))
		return nil
	},
}
