package cmd

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/loranna/configuration/device"
	"gitlab.com/loranna/loranna/loriot"
	"gopkg.in/yaml.v2"
)

var loriotserverurl, loriotserverapikey string
var deviceRegisterConfig = viper.New()

var illegalflags = []string{"deveui", "appeui",
	"appkey", "devaddr", "appskey", "nwkskey",
	"deviceserverport"}

func init() {
	device.AddAndBindFlags(deviceRegisterCmd, deviceRegisterConfig)
	device.BindEnv(deviceRegisterConfig)
	deviceCmd.AddCommand(deviceRegisterCmd)
}

var deviceRegisterCmd = &cobra.Command{
	Use:   "register [count]",
	Short: "Registers device(s)",
	Long: fmt.Sprintf(`This command can register one or more devices. If count is not supplied it
will register a device in the supplied server using the supplied API key.
Otherwise, in batch mode, it will register count number of devices with
random unique values hence the following flags are illegal in this mode:
%s`, strings.Join(illegalflags, ", ")),
	RunE: func(cmd *cobra.Command, args []string) error {
		c, err := device.LoadConfig(deviceRegisterConfig)
		if err != nil {
			return err
		}
		url := c.LoriotServerURL
		if len(url) == 0 {
			return fmt.Errorf("loriot server url is not set")
		}
		apikey := c.LoriotServerAPIKey
		if len(apikey) == 0 {
			return fmt.Errorf("loriot server api key is not set")
		}
		if len(args) < 1 {
			// normal mode
			_, err := os.Stat(defaultDeviceFileName)
			if err != nil {
				if os.IsNotExist(err) {
					registerandcreate(url, apikey, c)
				} else {
					return err
				}
			} else {
			question:
				logrus.Infof("%s exist. do you want to overwrite? [Y/n]\n", defaultDeviceFileName)
				reader := bufio.NewReader(os.Stdin)
				text, err := reader.ReadString('\n')
				if err != nil {
					return err
				}
				switch strings.TrimSpace(text) {
				default:
					logrus.Infoln("unknown input, try again")
					goto question
				case "y", "Y", "":
					err := registerandcreate(url, apikey, c)
					if err != nil {
						return err
					}
					logrus.Infof("%s was overwritten.\n", defaultDeviceFileName)
				case "n", "N":
					logrus.Infof("%s was left intact, no new device was registered\n", defaultDeviceFileName)
				}
			}
		} else {
			logrus.Infoln("batch mode enabled")
			configdir, err := os.Stat(defaultDeviceDirectory)
			if err != nil {
				if os.IsNotExist(err) {
					err := registerandcreatemulti(0, cmd, args)
					if err != nil {
						return err
					}
				} else {
					return err
				}
			} else {
				if configdir.IsDir() {
					dirfiles, err := ioutil.ReadDir(defaultDeviceDirectory)
					if err != nil {
						return err
					}
					if len(dirfiles) == 0 {
						err := registerandcreatemulti(len(dirfiles), cmd, args)
						if err != nil {
							return err
						}
					} else {
					batch_question:
						logrus.Infof("do you want to overwrite/append %s? [Y/n/a]\n", defaultDeviceDirectory)
						reader := bufio.NewReader(os.Stdin)
						text, err := reader.ReadString('\n')
						if err != nil {
							return err
						}
						switch strings.TrimSpace(text) {
						default:
							logrus.Infoln("unknown input, try again")
							goto batch_question
						case "y", "Y", "":
							os.RemoveAll(defaultDeviceDirectory)
							err := registerandcreatemulti(0, cmd, args)
							if err != nil {
								return err
							}
							logrus.Infof("%s directory was overwritten.\n", defaultDeviceDirectory)
						case "n", "N":
							logrus.Infof("%s was left intact, no new device was registered\n", defaultDeviceDirectory)
						case "a", "A":
							err := registerandcreatemulti(len(dirfiles), cmd, args)
							if err != nil {
								return err
							}
						}
					}
				} else {
					return fmt.Errorf("%s must be a directory", configdir.Name())
				}
			}
		}
		return nil
	},
}

func registerandcreate(url, apikey string, c *device.Config) error {
	switch c.Joinmethod {
	case "ABP":
		err := loriot.RegisterABPDevice(url, apikey,
			c.Deveui, c.Devaddr,
			c.Nwkskey, c.Appskey, c.Title,
			c.Counterup, c.Counterdown)
		if err != nil {
			return err
		}
	case "OTAA":
		err := loriot.RegisterOTAADevice(url, apikey,
			c.Deveui, c.Appeui,
			c.Appkey, c.Title)
		if err != nil {
			return err
		}
	default:
		return fmt.Errorf("unknown join method")
	}
	logrus.Infof("device with deveui %s is registered on %s as %s", c.Deveui, c.LoriotServerURL, c.Joinmethod)
	//float64 is not handled well in the serialization
	// https://github.com/spf13/viper/issues/894
	// so struct gets serialized instead
	//err = deviceRegisterConfig.WriteConfigAs(defaultDeviceFileName)
	//if err != nil {
	//	return err
	//}
	content, err := yaml.Marshal(c)
	if err != nil {
		return err
	}
	f, err := os.Create(defaultDeviceFileName)
	if err != nil {
		return err
	}
	defer f.Close()
	_, err = f.Write(content)
	if err != nil {
		return err
	}
	return nil
}

func registerandcreatemulti(firstid int, cmd *cobra.Command, args []string) error {
	count, err := strconv.Atoi(args[0])
	if err != nil {
		return err
	}
	os.MkdirAll(defaultDeviceDirectory, 0755)
	for i := firstid; i < firstid+count; i++ {
		vip := viper.New()
		dummycmd := &cobra.Command{}
		err = dummycmd.ParseFlags(args)
		if err != nil {
			return err
		}
		device.AddAndBindFlags(dummycmd, vip)
		device.BindEnv(vip)
		err = dummycmd.Execute()
		if err != nil {
			return err
		}
		c, err := device.LoadConfig(vip)
		if err != nil {
			return err
		}
		illegalflag := ""
		cmd.Flags().Visit(func(f *pflag.Flag) {
			for _, flag := range illegalflags {
				if flag == f.Name {
					illegalflag = flag
					return
				}
			}
		})
		if len(illegalflag) > 0 {
			return fmt.Errorf("cannot use flag %s in batch mode", illegalflag)
		}
		confbytes, err := yaml.Marshal(c)
		if err != nil {
			return err
		}
		err = ioutil.WriteFile(path.Join(defaultDeviceDirectory, fmt.Sprintf("%03d.yml", i)), confbytes, 0755)
		if err != nil {
			return err
		}
		switch c.Registermethod {
		case "none", "other":
		case "self", "auto":
			switch c.Joinmethod {
			case "ABP":
				err = loriot.RegisterABPDevice(c.LoriotServerURL, c.LoriotServerAPIKey,
					c.Deveui, c.Devaddr, c.Nwkskey, c.Appskey,
					c.Title, c.Counterup, c.Counterdown)
				if err != nil {
					return err
				}
			case "OTAA":
				err = loriot.RegisterOTAADevice(c.LoriotServerURL, c.LoriotServerAPIKey,
					c.Deveui, c.Appeui, c.Appkey, c.Title)
				if err != nil {
					return err
				}
			}
		}
		logrus.Infof("%d/%d (deveui %s) created", i+1, firstid+count, c.Deveui)
		device.Regenerate()
	}
	return nil
}
