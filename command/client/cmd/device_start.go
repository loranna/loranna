package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"path"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/loranna/configuration/device"
)

var deviceStartConfig = viper.New()
var batchsize uint
var batchdelay, batchrandomdelay float64
var keepconfig bool
var localconfig = viper.New()

func init() {
	deviceStartCmd.Flags().UintVar(&batchsize, "batchsize", 0, "Number of devices that should be started at once")
	localconfig.BindPFlag("batchsize", deviceStartCmd.Flags().Lookup("batchsize"))
	localconfig.BindEnv("batchsize", "BATCH_SIZE")
	deviceStartCmd.Flags().Float64Var(&batchdelay, "batchdelay", 0, "Wait time between device batch starts in seconds")
	localconfig.BindPFlag("batchdelay", deviceStartCmd.Flags().Lookup("batchdelay"))
	localconfig.BindEnv("batchdelay", "BATCH_DELAY")
	deviceStartCmd.Flags().Float64Var(&batchrandomdelay, "batchrandomdelay", 0, "Maximum random delay between device batch starts in seconds")
	localconfig.BindPFlag("batchrandomdelay", deviceStartCmd.Flags().Lookup("batchrandomdelay"))
	localconfig.BindEnv("batchrandomdelay", "BATCH_RANDOM_DELAY")
	deviceStartCmd.Flags().BoolVar(&keepconfig, "keepconfig", false, "Keep the configuration file(s) after using them")
	localconfig.BindPFlag("keepconfig", deviceStartCmd.Flags().Lookup("keepconfig"))
	localconfig.BindEnv("keepconfig", "KEEP_CONFIG")
	device.AddAndBindFlags(deviceStartCmd, deviceStartConfig)
	device.BindEnv(deviceStartConfig)
	deviceCmd.AddCommand(deviceStartCmd)
}

const defaultDeviceFileName = "device_config.yml"
const defaultDeviceDirectory = "dev_configs"

var deviceStartCmd = &cobra.Command{
	Use:   "start [path]",
	Short: "Starts device(s) based on the existing config file(s)",
	Long: fmt.Sprintf(`The command looks for %s and sends that config as a device to start.
If no config file, it will look for directory %s and assumes that
all files are valid configs for devices and send them over to the daemon
`, defaultDeviceFileName,defaultDeviceDirectory),
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			//try default file
			_, err := os.Stat(defaultDeviceFileName)
			if err != nil {
				if os.IsNotExist(err) {
					//try default directory
					configdir, err := os.Stat(defaultDeviceDirectory)
					if err != nil {
						if os.IsNotExist(err) {
							c, err := device.LoadConfig(deviceStartConfig)
							if err != nil {
								return err
							}
							switch c.Registermethod {
							case "auto", "self":
								logrus.Infof("%s registration", c.Registermethod)
							default:
								return fmt.Errorf("invalid register method %s", c.Registermethod)
							}
						} else {
							return err
						}
					} else {
						if configdir.IsDir() {
							//process config files in directory
							err := processdir(defaultDeviceDirectory)
							if err != nil {
								return err
							}
						} else {
							return fmt.Errorf("%s must be a directory", configdir.Name())
						}
					}
				} else {
					return err
				}
			} else {
				//process default config file
				err := processfile(defaultDeviceFileName)
				if err != nil {
					return err
				}
			}

		} else {
			info, err := os.Stat(args[0])
			if err != nil {
				return err
			}
			if info.IsDir() {
				//batch mode
				err := processdir(args[0])
				if err != nil {
					return err
				}
			} else {
				//normal mode
				err := processfile(args[0])
				if err != nil {
					return err
				}
			}
		}
		if !localconfig.GetBool("keepconfig") {
			if len(args) > 0 {
				os.RemoveAll(args[0])
			}
			os.RemoveAll(defaultDeviceFileName)
			os.RemoveAll(defaultDeviceDirectory)
		}
		return nil
	},
}

func start(conf []byte) error {
	reader := bytes.NewReader(conf)
	daemonurl := fmt.Sprintf("http://%s:%d/device/start", rootViper.GetString("serveraddress"), rootViper.GetUint("serverport"))
	r, err := http.Post(daemonurl, "application/json", reader)
	if err != nil {
		return err
	}
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	fmt.Print(string(b))
	return nil
}

func processfile(name string) error {
	deviceStartConfig.SetConfigType("yaml")
	deviceStartConfig.SetConfigFile(name)
	err := deviceStartConfig.ReadInConfig()
	if err != nil {
		return err
	}
	c, err := device.LoadConfig(deviceStartConfig)
	if err != nil {
		return err
	}
	conf, err := json.Marshal(c)
	if err != nil {
		return err
	}
	err = start(conf)
	if err != nil {
		return err
	}
	logrus.Infof("file %s processed", name)
	return nil
}

func processdir(name string) error {
	files, err := ioutil.ReadDir(name)
	if err != nil {
		return err
	}
	random := rand.New(rand.NewSource(time.Now().UnixNano()))
	bsize := localconfig.GetUint("batchsize")
	d := localconfig.GetFloat64("batchdelay")
	rd := localconfig.GetFloat64("batchrandomdelay")
	for i, file := range files {
		err = processfile(path.Join(name, file.Name()))
		if err != nil {
			return err
		}
		logrus.Infof("%d/%d started", i+1, len(files))
		if bsize > 0 {
			if uint(i+1)%bsize == 0 {
				delaySec := d + random.Float64()*rd
				delayNanoSec := int64(delaySec * 1_000_000_000)
				logrus.Infof("delaying next batch for %.03f seconds", delaySec)
				time.Sleep(time.Duration(delayNanoSec) * time.Nanosecond)
			}
		}
	}
	return nil
}
