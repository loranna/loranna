package cmd

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var rootViper = viper.New()
var serveraddress string
var serverport uint
var checkbroker, checkenvironment, checkloriot bool

func init() {
	rootCmd.PersistentFlags().StringVar(&serveraddress, "serveraddress", "localhost", "URL of the Lorannad server")
	rootViper.BindPFlag("serveraddress", rootCmd.PersistentFlags().Lookup("serveraddress"))
	rootViper.BindEnv("serveraddress", "SERVER_ADDRESS")
	rootCmd.PersistentFlags().UintVar(&serverport, "serverport", 35998, "Port of the Lorannad server")
	rootViper.BindPFlag("serverport", rootCmd.PersistentFlags().Lookup("serverport"))
	rootViper.BindEnv("serverport", "SERVER_PORT")
}

var rootCmd = &cobra.Command{
	Use:           "loranna",
	Short:         "manages a Loranna simulation",
	DisableAutoGenTag: true,
	SilenceErrors: false,
	SilenceUsage:  true,
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		logrus.Fatal(err)
	}
}
