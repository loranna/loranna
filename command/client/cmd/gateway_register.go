package cmd

import (
	"bufio"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/loranna/loranna/daemon/api/message"
	"gitlab.com/loranna/loranna/loriot"
)

var gatewayRegisterConfig = viper.New()

func init() {
	gatewayRegisterCmd.Flags().StringVar(&loriotserverurl, "loriotserverurl", "", "URL of the LORIOT server used for data processing")
	gatewayRegisterConfig.BindPFlag("loriotserverurl", gatewayRegisterCmd.Flags().Lookup("loriotserverurl"))
	gatewayRegisterConfig.BindEnv("loriotserverurl", "LORIOT_SERVER_URL")
	gatewayRegisterCmd.Flags().StringVar(&loriotserverapikey, "loriotserverapikey", "", "API key of the LORIOT server used for data processing")
	gatewayRegisterConfig.BindPFlag("loriotserverapikey", gatewayRegisterCmd.Flags().Lookup("loriotserverapikey"))
	gatewayRegisterConfig.BindEnv("loriotserverapikey", "LORIOT_SERVER_API_KEY")
	gatewayCmd.AddCommand(gatewayRegisterCmd)
}

const defaultGatewayFileNameFormat = "gateway_config%s.yml"
const defaultGatewayFileName = "gateway_config.yml"
const defaultGatewaysDirectory = "gw_configs"

var gatewayRegisterCmd = &cobra.Command{
	Use:   "register [count]",
	Short: "Registers a gateway in LORIOT with the MAC address returned by the server",
	RunE: func(cmd *cobra.Command, args []string) error {
		count := 1
		if len(args) > 0 {
			c, err := strconv.Atoi(args[0])
			if err != nil {
				return err
			}
			count = c
		}
		if count > 1 && strings.TrimSpace(strings.ToLower(gatewayLocal.GetString("runner"))) == "native" {
			return errors.New("native runner does not support multiple gateways")
		}
		err := os.MkdirAll(defaultGatewaysDirectory, 0755)
		if err != nil {
			return err
		}
		for i := 0; i < count; i++ {
			daemonurl := fmt.Sprintf("http://%s:%d/gateway/info?runner=%s", rootViper.GetString("serveraddress"),
				rootViper.GetUint("serverport"), gatewayLocal.GetString("runner"))
			r, err := http.Get(daemonurl)
			if err != nil {
				return err
			}

			b, err := ioutil.ReadAll(r.Body)
			if err != nil {
				return err
			}
			info, err := message.UnmarshalGatewayInfo(b)
			if err != nil {
				return err
			}
			v := viper.New()
			v.Set("macaddress", info.MacAddress)
			v.Set("networkinterfacename", info.InterfaceName)
			url := gatewayRegisterConfig.GetString("loriotserverurl")
			if len(url) == 0 {
				return fmt.Errorf("loriot server url is not set")
			}
			apikey := gatewayRegisterConfig.GetString("loriotserverapikey")
			if len(apikey) == 0 {
				return fmt.Errorf("loriot server api key is not set")
			}
			_, err = os.Stat(path.Join(defaultGatewaysDirectory, fmt.Sprintf(defaultGatewayFileNameFormat, strconv.Itoa(i))))
			if err != nil {
				if os.IsNotExist(err) {
					err := creategateway(url, apikey, info.MacAddress, v, i)
					if err != nil {
						return err
					}
				} else {
					return err
				}
			} else {
			question:
				logrus.Infof("%s exist, do you want to overwrite? [Y/n]\n", path.Join(defaultGatewaysDirectory, fmt.Sprintf(defaultGatewayFileNameFormat, strconv.Itoa(i))))
				reader := bufio.NewReader(os.Stdin)
				text, err := reader.ReadString('\n')
				if err != nil {
					return err
				}
				switch strings.TrimSpace(text) {
				default:
					logrus.Infoln("unknown input, try again")
					goto question
				case "y", "Y", "":
					err := creategateway(url, apikey, info.MacAddress, v, i)
					if err != nil {
						return err
					}
					logrus.Infof("%s was overwritten.\n", path.Join(defaultGatewaysDirectory, fmt.Sprintf(defaultGatewayFileNameFormat, strconv.Itoa(i))))
				case "n", "N":
					logrus.Infof("%s was left intact, no new gateway was registered\n", path.Join(defaultGatewaysDirectory, fmt.Sprintf(defaultGatewayFileNameFormat, strconv.Itoa(i))))
				}
			}
		}
		return nil
	},
}

func creategateway(url, apikey, macaddress string, v *viper.Viper, i int) error {
	err := loriot.RegisterGateway(url, apikey, macaddress)
	if err != nil {
		return err
	}
	v.Set("loriotserverurl", gatewayRegisterConfig.GetString("loriotserverurl"))
	err = v.WriteConfigAs(path.Join(defaultGatewaysDirectory, fmt.Sprintf(defaultGatewayFileNameFormat, strconv.Itoa(i))))
	if err != nil {
		return err
	}
	return nil
}
