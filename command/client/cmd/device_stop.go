package cmd

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/loranna/loranna/daemon/api/message"
)

func init() {
	deviceCmd.AddCommand(deviceStopCmd)
}

var deviceStopCmd = &cobra.Command{
	Use:   "stop [id]",
	Short: "Stops the selected or all devices",
	RunE: func(cmd *cobra.Command, args []string) error {
		id := 0
		if len(args) > 0 {
			if args[0] == "all" {
				id = -1
			} else {
				n, err := strconv.Atoi(args[0])
				if err != nil {
					return err
				}
				id = n
			}
		}
		c, err := message.NewID(id)
		if err != nil {
			return err
		}
		reader := bytes.NewReader(c)
		daemonurl := fmt.Sprintf("http://%s:%d/device/stop", rootViper.GetString("serveraddress"), rootViper.GetUint("serverport"))
		r, err := http.Post(daemonurl, "application/json", reader)
		if err != nil {
			return err
		}
		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			return err
		}
		fmt.Print(string(b))
		return nil
	},
}
