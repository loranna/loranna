package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/loranna/configuration/gateway"
)

var gatewayStartViper = viper.New()
var localgatewayconfig = viper.New()

func init() {
	gatewayStartCmd.Flags().BoolVar(&keepconfig, "keepconfig", false, "Keep the configuration file(s) after using them")
	localgatewayconfig.BindPFlag("keepconfig", gatewayStartCmd.Flags().Lookup("keepconfig"))
	localgatewayconfig.BindEnv("keepconfig", "KEEP_CONFIG")
	gateway.AddAndBindFlags(gatewayStartCmd, gatewayStartViper)
	gateway.BindEnv(gatewayStartViper)
	gatewayCmd.AddCommand(gatewayStartCmd)
}

var gatewayStartCmd = &cobra.Command{
	Use:   "start [path]",
	Short: "Starts the gateway(s) using the default or the supplied config file(s)",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) == 1 {
			f, err := os.Stat(args[0])
			if err != nil {
				if os.IsNotExist(err) {
					return err
				} else {
					if f.IsDir() {
						//process files in directory
						files, err := ioutil.ReadDir(args[0])
						if err != nil {
							return err
						}
						for i := 0; i < len(files); i++ {
							filename := files[i].Name()
							gatewayStartViper.SetConfigFile(filename)
							err := gatewayStartViper.ReadInConfig()
							if err != nil {
								return err
							}
							err = startGateway()
							if err != nil {
								return err
							}
						}
					} else {
						gatewayStartViper.SetConfigFile(args[0])
						err := gatewayStartViper.ReadInConfig()
						if err != nil {
							return err
						}
					}
				}
			}
		} else {
			f, err := os.Stat(defaultGatewayFileName)
			if err != nil {
				if os.IsNotExist(err) {
					f, err := os.Stat(defaultGatewaysDirectory)
					if err != nil {
						if os.IsNotExist(err) {
							return fmt.Errorf("no default file or default directory exist")
						}
						return err
					} else {
						//process directory
						files, err := ioutil.ReadDir(f.Name())
						if err != nil {
							return err
						}
						for i := 0; i < len(files); i++ {
							filename := files[i].Name()
							fmt.Println(filename)
							gatewayStartViper.SetConfigFile(path.Join(defaultGatewaysDirectory, filename))
							err := gatewayStartViper.ReadInConfig()
							if err != nil {
								return err
							}
							err = startGateway()
							if err != nil {
								return err
							}
						}
					}
				} else {
					return err
				}
			} else {
				gatewayStartViper.SetConfigFile(f.Name())
				err = gatewayStartViper.ReadInConfig()
				if err != nil {
					return err
				}
			}
		}
		if !localgatewayconfig.GetBool("keepconfig") {
			if len(args) > 0 {
				os.RemoveAll(args[0])
			}
			os.RemoveAll(defaultGatewayFileName)
		}
		return nil
	},
}

func startGateway() error {
	c, err := gateway.LoadConfig(gatewayStartViper)
	if err != nil {
		return fmt.Errorf("could not load gateway config: %w", err)
	}
	conf, err := json.Marshal(c)
	if err != nil {
		return err
	}
	fmt.Println(string(conf))
	reader := bytes.NewReader(conf)
	daemonurl := fmt.Sprintf("http://%s:%d/gateway/start?runner=%s",
		rootViper.GetString("serveraddress"), rootViper.GetUint("serverport"),
		gatewayLocal.GetString("runner"))
	r, err := http.Post(daemonurl, "application/json", reader)
	if err != nil {
		return err
	}
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	fmt.Print(string(b))
	return nil
}
