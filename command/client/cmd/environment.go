package cmd

import (
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(environmentCmd)
}

var environmentCmd = &cobra.Command{
	Use:   "environment",
	Short: "environment related commands",
}
