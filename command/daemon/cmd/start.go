package cmd

import (
	"context"
	"fmt"
	"math"
	"net/http"
	"os"
	"os/signal"
	"path"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/loranna/loranna/daemon/api"
	"gitlab.com/loranna/loranna/daemon/process"
	"gitlab.com/loranna/loranna/daemon/process/device"
	"gitlab.com/loranna/loranna/daemon/process/environment"
	"gitlab.com/loranna/loranna/daemon/process/gateway/loriotpf"
)

var serveraddress string
var serverport uint

func init() {
	startCmd.Flags().StringVar(&serveraddress, "serveraddress", "", "URL of the Lorannad server")
	viper.BindPFlag("serveraddress", startCmd.Flags().Lookup("serveraddress"))
	viper.BindEnv("serveraddress", "SERVER_ADDRESS")
	startCmd.Flags().UintVar(&serverport, "serverport", 35998, "Port of the Lorannad server")
	viper.BindPFlag("serverport", startCmd.Flags().Lookup("serverport"))
	viper.BindEnv("serverport", "SERVER_PORT")
	rootCmd.AddCommand(startCmd)
}

var errorChannel = make(chan error, 100)

var startCmd = &cobra.Command{
	Use:           "start",
	Short:         "manages a loranna simulation",
	SilenceErrors: false,
	SilenceUsage:  true,
	RunE: func(cmd *cobra.Command, args []string) error {
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)

		h := api.NewHandlers(logrus.StandardLogger(), errorChannel)

		mx := mux.NewRouter()
		h.SetupRoutes(mx)
		defer h.Close()
		addr := fmt.Sprintf("%s:%d", viper.GetString("serveraddress"), viper.GetUint("serverport"))
		server := &http.Server{Addr: addr, Handler: mx}

		go starthttpserver(errorChannel, server)
		defer server.Shutdown(context.Background())

		logrus.Infoln("lorannad started.")
		_, err := os.Stat(path.Join(process.BinPath, environment.BinName))
		if err != nil {
			if os.IsNotExist(err) {
				logrus.Infoln("updating environment.")
				e := environment.New(logrus.StandardLogger())
				err = e.Update("")
				if err != nil {
					return err
				}
				e.Close()
			} else {
				return err
			}
		}
		e := environment.New(logrus.StandardLogger())
		v, err := e.Version()
		if err != nil {
			return err
		}
		e.Close()
		logrus.Infof("current environment version: %s", v)

		_, err = os.Stat(path.Join(process.BinPath, loriotpf.BinName))
		if err != nil {
			if os.IsNotExist(err) {
				logrus.Infoln("updating gateway.")
				pf := loriotpf.New(logrus.StandardLogger())
				err = pf.Update("")
				if err != nil {
					return err
				}
				pf.Close()
			} else {
				return err
			}
		}
		pf := loriotpf.New(logrus.StandardLogger())
		v, err = pf.Version()
		if err != nil {
			return err
		}
		pf.Close()
		logrus.Infof("current gateway version: %s", v)

		_, err = os.Stat(path.Join(process.BinPath, device.BinName))
		if err != nil {
			if os.IsNotExist(err) {
				logrus.Infoln("updating device.")
				dev := device.New(logrus.StandardLogger(), math.MaxInt64)
				err = dev.Update("")
				if err != nil {
					return err
				}
				dev.Close()
			} else {
				return err
			}
		}
		dev := device.New(logrus.StandardLogger(), math.MaxInt64)
		v, err = dev.Version()
		if err != nil {
			return err
		}
		dev.Close()
		logrus.Infof("current device version: %s", v)

		logrus.Infoln("finished checking components.")

		select {
		case <-c:
			return nil
		case e := <-errorChannel:
			return e
		}
	},
}

func starthttpserver(err chan<- error, server *http.Server) {
	e := server.ListenAndServe()
	if e != http.ErrServerClosed {
		err <- e
	}
}
