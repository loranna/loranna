#! /bin/bash

fpm -s dir -t deb -v 0.9.0 -n librabbitmq4 --maintainer "Tamás Király <kiraly.tamas@outlook.com>" --vendor "Tamás Király <kiraly.tamas@outlook.com>" --after-install after-install.sh --url https://rabbitmq.org --description "rabbitmq library" --license MIT ./librabbitmq/lib=/usr/local
fpm -s dir -t deb -v 1.3.2 -n libprotobuf-c1 --maintainer "Tamás Király <kiraly.tamas@outlook.com>" --vendor "Tamás Király <kiraly.tamas@outlook.com>" --after-install  after-install.sh --url https://protobuf.org --description "protobuf-c library" --license MIT ./libprotobuf-c/lib=/usr/local/

package_cloud push tkiraly/loranna/ubuntu/bionic *.deb