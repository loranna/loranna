# CHANGELOG

- lorannad starts broker automatically, no need for `loranna broker start` command
- if gateway is running in docker, it now knows the ip of the host machine (where broker and environment runs) so no need to set `rabbitmqserverurl` and `environmentserverurl`
