# Loranna Project

## Loranna daemon

Lorannad is a service which exposes an HTTP server to manipulate the loranna simulation

## Loranna client

Loranna is a CLI which access Lorannad's HTTP server and helps manipulate the simulation

## Prerequirements

- root user of the machine running the systemd service must have pull access to docker repository `registry.gitlab.com`
do `sudo su` and `docker login registry.gitlab.com`

## Installation

- add repo: `curl -s https://packagecloud.io/install/repositories/tkiraly/loranna/script.deb.sh | sudo bash`
- install: `apt install loranna`

## Quickstart

- Loranna should already runnning as a systemd service. `loranna` command is a cli to manage loranna simulation.
- check for status `sudo service loranna status`

## Spin up a simulation

- `loranna broker start` # you have to wait until rabbitmq is started properly `loranna broker status` must return "running"
- `loranna environment start`
- `loranna gateway register --loriotserverurl LORIOT_SERVER_URL --loriotserverapikey LORIOT_API_KEY`
- `loranna gateway start`
- `loranna device register 5 --joinmethod ABP --loriotserverurl LORIOT_SERVER_URL --loriotserverapikey LORIOT_API_KEY`
- `loranna device start --batchsize 2 --batchrandomdelay 2`

## Troubleshoot

- `journalctl -u loranna -f` # shows daemon logs
- systemd service stores everything in `/var/lib/lorannad` look for log/config there

## Limitations

- `sudo service loranna restart` must run to remove the junk from previous runs

## Optional

- update individual components: `loranna [component] update [version]` e.g.: `loranna environment update 0.11.147`

## Bash completion usage

- make sure you have `bash-completion` package
- copy `bash.sh` and rename it to `vdevice` to bash-completion directory
- make sure functionality is enabled in `/etc/bash.bashrc` or `~/.bashrc`
